#!/usr/bin/bash

echo "Formatting Source Files..."

find ! -path "*/include/*" -name '*.[ch]' -type f -print0 | xargs -0 --max-args=1 --max-procs=8 indent -lp -bad -bap -bbb -bl -bli4 -cdw -cli0 -ss -pcs -ncs -saf -sai -saw -di4 -nbc -psl -bls -blf -i4 -bs --line-length120 --ignore-newlines -pmt -nut
