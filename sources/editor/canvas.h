#ifndef _CANVAS_H_
#define _CANVAS_H_

#include <gtk/gtk.h>

#include "../include/opengl.h"

#include <gtkgl/gtkglarea.h>


int se_init_gl_canvas (GtkWidget * widget);
GtkWidget *se_create_gl_canvas ();
int draw (GtkWidget * widget, GdkEventExpose * event);
int reshape (GtkWidget * widget, GdkEventConfigure * event);


#endif
