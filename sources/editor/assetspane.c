#include "assetspane.h"


GtkWidget *
se_create_assets_pane ()
{
    GtkWidget *assets_pane;
    GtkWidget *frame;
    GtkWidget *label;
    int i;

    char bufferf[32];
    char bufferl[32];

    /* Creates the notebook widget */
    assets_pane = gtk_notebook_new ();

    /* Sets tabs on bottom */
    gtk_notebook_set_tab_pos (GTK_NOTEBOOK (assets_pane), GTK_POS_TOP);
    gtk_notebook_set_show_tabs (GTK_NOTEBOOK (assets_pane), TRUE);
    gtk_notebook_set_show_border (GTK_NOTEBOOK (assets_pane), TRUE);


    for (i = 0; i < 5; i++)
        {
            sprintf (bufferf, "Append Frame %d", i + 1);
            sprintf (bufferl, "Page %d", i + 1);

            frame = gtk_frame_new (bufferf);
            gtk_container_set_border_width (GTK_CONTAINER (frame), 10);
            gtk_widget_set_size_request (frame, 100, 75);
            gtk_widget_show (frame);

            label = gtk_label_new (bufferf);
            gtk_container_add (GTK_CONTAINER (frame), label);
            gtk_widget_show (label);

            label = gtk_label_new (bufferl);
            gtk_notebook_prepend_page (GTK_NOTEBOOK (assets_pane), frame, label);
        }

    gtk_widget_set_size_request (assets_pane, 230, -1);

    return assets_pane;
}
