#include "setoolbar.h"


GtkWidget *
se_toolbar_new ()
{
    GtkWidget *toolbar;
    GtkWidget *button;
    GtkWidget *icon;



    /* Create a new toolbar widget */
    toolbar = gtk_toolbar_new ();
    gtk_toolbar_set_orientation (GTK_TOOLBAR (toolbar), GTK_ORIENTATION_HORIZONTAL);
    gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_ICONS);
    gtk_container_set_border_width (GTK_CONTAINER (toolbar), 0);
    /*gtk_toolbar_set_space_size(GTK_TOOLBAR(toolbar), 5); */

    /* Setup the file i/o options */

    /* Create New Map button */
    icon = gtk_image_new_from_file ("data/icons/map.png");
    button =
        gtk_toolbar_append_item (GTK_TOOLBAR (toolbar), "New Map", "Create a new map", "Private", icon, NULL, NULL);

    /* Create Open Map button */
    icon = gtk_image_new_from_file ("data/icons/folder.png");
    button =
        gtk_toolbar_append_item (GTK_TOOLBAR (toolbar), "Open", "Open an existing map", "Private", icon, NULL, NULL);

    /* Create Save Map button */
    icon = gtk_image_new_from_file ("data/icons/disk.png");
    button = gtk_toolbar_append_item (GTK_TOOLBAR (toolbar), "Save", "save map", "Private", icon, NULL, NULL);



    /* Setup the undo/redo options */
    gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));


    /* Create the undo button */
    icon = gtk_image_new_from_file ("data/icons/arrow_undo.png");
    button = gtk_toolbar_append_item (GTK_TOOLBAR (toolbar), "Undo", "Undo last action", "Private", icon, NULL, NULL);

    /* Create the redo button */
    icon = gtk_image_new_from_file ("data/icons/arrow_redo.png");
    button = gtk_toolbar_append_item (GTK_TOOLBAR (toolbar), "Redo", "Redo last action", "Private", icon, NULL, NULL);



    /* Setup the grid and zoom options */
    gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));

    /* Create toggle grid button */
    icon = gtk_image_new_from_file ("data/icons/grid.png");
    button =
        gtk_toolbar_append_element (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_CHILD_TOGGLEBUTTON, NULL, "Toggle Grid",
                                    "Toggle the Grid", "Private", icon, NULL, NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), TRUE);

    /* Create the zoom in button */
    icon = gtk_image_new_from_file ("data/icons/zoom_in.png");
    button = gtk_toolbar_append_item (GTK_TOOLBAR (toolbar), "Zoom In", "Zoom In", "Private", icon, NULL, NULL);


    icon = gtk_image_new_from_file ("data/icons/zoom_out.png");
    button = gtk_toolbar_append_item (GTK_TOOLBAR (toolbar), "Zoom Out", "Zoom Out", "Private", icon, NULL, NULL);


    /* Setup the paint tool option */
    gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));

    /* Create the pencil tool button */
    icon = gtk_image_new_from_file ("data/icons/pencil.png");
    button =
        gtk_toolbar_append_element (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_CHILD_TOGGLEBUTTON, NULL, "Pencil", "Use Pencil",
                                    "Private", icon, NULL, NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), TRUE);


    /* Create eraser tool button */
    icon = gtk_image_new_from_file ("data/icons/draw_eraser.png");
    button =
        gtk_toolbar_append_element (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_CHILD_TOGGLEBUTTON, NULL, "Eraser", "Use Eraser",
                                    "Private", icon, NULL, NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), FALSE);


    /* Create bucket tool button */
    icon = gtk_image_new_from_file ("data/icons/paintcan.png");
    button =
        gtk_toolbar_append_element (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_CHILD_TOGGLEBUTTON, NULL, "Paint Bucket",
                                    "Use Paint Bucket", "Private", icon, NULL, NULL);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), FALSE);


    /* Setup the layer options */
    gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));
    return toolbar;
}
