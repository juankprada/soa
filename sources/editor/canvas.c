#include "canvas.h"

#include <math.h>

#include "../engine/spritebatch.h"


int
se_init_gl_canvas (GtkWidget * widget)
{
    /* glViewport (0, 0, widget->allocation.width, widget->allocation.height); */
    /* glViewport (0, 0, 800, 600);
     * glMatrixMode (GL_PROJECTION);
     * glLoadIdentity ();
     * glOrtho (0, 800, 0, 600, -1, 1);
     * glMatrixMode (GL_MODELVIEW);
     * glLoadIdentity ();
     * glTranslatef (0.0f, 0.0f, 0.0f); */


    GLenum err = glewInit ();

    if (GLEW_OK != err)
        {
            /* Problem: glewInit failed, something is seriously wrong. */
            fprintf (stderr, "Error initializing GLEW: %s\n", glewGetErrorString (err));
            return;
        }

    fprintf (stdout, "Status: Using GLEW %s\n", glewGetString (GLEW_VERSION));

    /* set Clear color to a soft blue */
    glClearColor (0.4, 0.6, 0.9, 1);

    glViewport (0, 0, widget->allocation.width, widget->allocation.height);

    sprite_batch_setup ();
    camera_create ();
    camera_set_position_coord (0, 0);




    return TRUE;
}

int
draw (GtkWidget * widget, GdkEventExpose * event)
{

    if (event->count > 0)
        return TRUE;



    if (gtk_gl_area_make_current (GTK_GL_AREA (widget)))
        {

            sprite_batch_begin ();
            camera_set_position_coord (0, 0);
            glClearColor (1, 0, 0, 1);
            glClear (GL_COLOR_BUFFER_BIT);
            glColor3f (1, 1, 1);
            glBegin (GL_QUADS);
            glVertex2f (0, 0);
            glVertex2f (0, 20);
            glVertex2f (20, 20);
            glVertex2f (20, 0);
            glEnd ();
            gtk_gl_area_swap_buffers (GTK_GL_AREA (widget));

            sprite_batch_end ();
        }

    return TRUE;
}

int
reshape (GtkWidget * widget, GdkEventConfigure * event)
{
    gtk_gl_area_make_current (GTK_GL_AREA (widget));
    /* glViewport (0, 0, 800, 600); */
    /* glViewport (0, 0, widget->allocation.width, widget->allocation.height); */
    return TRUE;
}
