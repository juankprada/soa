#include "main_window.h"

#include "setoolbar.h"
#include "canvas.h"
#include "assetspane.h"
#include <gtk/gtk.h>
#include <GL/gl.h>
#include <gtkgl/gtkglarea.h>

static GtkWidget *window;

static int init_components ();



static gboolean
delete_event (GtkWidget * widget, GdkEvent * event, gpointer data)
{
    gtk_main_quit ();
    return FALSE;
}


void
se_create_main_window (int width, int height)
{

    if (gdk_gl_query () == FALSE)
        return;

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (window), "SOA Engine Game Editor v.0.1a");
    gtk_window_set_default_size (GTK_WINDOW (window), width, height);

    init_components ();

    gtk_widget_show_all (window);

    gtk_main ();
}



int
init_components ()
{
    GtkWidget *toolbar;
    GtkWidget *handlebox;
    GtkWidget *vbox;
    GtkWidget *glarea;
    GtkWidget *h_paned_widget;
    GtkWidget *assets_pane;

    int attrlist[] = {
        GDK_GL_RGBA,
        GDK_GL_RED_SIZE, 1,
        GDK_GL_GREEN_SIZE, 1,
        GDK_GL_BLUE_SIZE, 1,
        GDK_GL_DOUBLEBUFFER,
        GDK_GL_NONE
    };

    gtk_quit_add_destroy (1, GTK_OBJECT (window));

    glarea = GTK_WIDGET (gtk_gl_area_new (attrlist));
    gtk_widget_set_size_request (GTK_WIDGET (glarea), 100, 100);

    gtk_widget_set_events (GTK_WIDGET (glarea), GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK);

    g_signal_connect (glarea, "expose_event", G_CALLBACK (draw), NULL);
    g_signal_connect (glarea, "configure_event", G_CALLBACK (reshape), NULL);
    g_signal_connect (glarea, "realize", G_CALLBACK (se_init_gl_canvas), NULL);



    /* Set the close callback function */
    g_signal_connect (window, "delete-event", G_CALLBACK (delete_event), NULL);

    /* Realize the window due to pixmaps being used */
    gtk_widget_realize (window);

    vbox = gtk_vbox_new (FALSE, 0);
    gtk_container_add (GTK_CONTAINER (window), vbox);


    /* Create a handlebox to store the toolbar */
    handlebox = gtk_handle_box_new ();
    gtk_box_pack_start (GTK_BOX (vbox), handlebox, FALSE, FALSE, 0);



    /* Creates the hPaned widget that will store the assets pane and the canvas */
    assets_pane = se_create_assets_pane ();


    h_paned_widget = gtk_hpaned_new ();

    gtk_paned_pack1 (GTK_PANED (h_paned_widget), assets_pane, TRUE, FALSE);
    gtk_paned_pack2 (GTK_PANED (h_paned_widget), glarea, TRUE, FALSE);

    gtk_box_pack_start (GTK_BOX (vbox), h_paned_widget, TRUE, TRUE, 0);


    gtk_widget_show (glarea);


    /* Create a new SOA Editor toolbar */
    toolbar = se_toolbar_new ();

    /* Adds the toolbar to main window */
    gtk_container_add (GTK_CONTAINER (handlebox), toolbar);

    gtk_widget_show (vbox);
    gtk_widget_show (toolbar);
    gtk_widget_show (handlebox);

}
