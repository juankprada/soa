
/*
 *  main.c
 *
 *  Created on: Sep 16, 2012
 *  Author: Juankprada
 */

#include <stdio.h>

#include "../engine/game.h"
#include "../engine/rsort.h"
#include "playstate.h"


main (int argc, char **argv)
{


    soa_create_game_window ();

    playstate_setup ();

    playstate_register ();

    soa_run_game ();

    return 0;
}
