#include "../engine/opengl.h"
#include "../engine/graphics.h"
#include "../engine/game.h"
#include "../engine/spritebatch.h"
#include "../engine/entity.h"
#include "../engine/camera.h"
#include "../engine/list.h"
#include "../engine/rsort.h"
#include "../engine/collisions.h"
#include "../engine/logging.h"

#include "playstate.h"


GameState playState;

static Entity player;
static Entity npc, npc2;

static List entities_list; 

void playstate_init ();
void playstate_handle_keyboard (GLFWwindow * window, int key, int scancode, int action, int mods);
void playstate_update ();
void playstate_render (double interpolation);
void playstate_finalize ();

void entity_custom_b(EntityPtr entity);

void
playstate_register ()
{
    soa_set_game_state (&playState);

}


void
playstate_setup ()
{
    playState.on_init = &playstate_init;
    playState.on_finish = &playstate_finalize;
    playState.on_render = &playstate_render;
    playState.on_update = &playstate_update;

}


void
playstate_init ()
{
    log_message(INFO, "Initializing Play state\n");

    sprite_batch_setup ();
    /* create_entity ("Finwe Isilra", &entity); */
    load_entity_from_xml("data/xml/entity1.xml", &player);
    load_entity_from_xml("data/xml/entity1.xml", &npc);
    load_entity_from_xml("data/xml/entity1.xml", &npc2);
    

    player.custom_behaviour = &entity_custom_b;
    npc.custom_behaviour = &entity_custom_b;
    npc2.custom_behaviour = &entity_custom_b;
    /* create_entity ("NPC", &npc);
     * create_entity ("NPC", &npc2) */;



    /* npc = entity; */

    list_init (&entities_list, NULL);

    
    list_add(&entities_list, &npc);
    list_add(&entities_list, &player);
    list_add(&entities_list, &npc2);
    
    /* list_insert_next (&entities_list, NULL, &npc);
     * list_insert_next (&entities_list, list_head (&entities_list), &entity);
     * list_insert_next (&entities_list, list_head (&entities_list), &npc2); */

    entity_set_position(&npc, 100, 0);
    /* npc.position.x = 100;
     * npc.position.y = 0;
     * npc.colBox.x = 100;
     * npc.colBox.y = 0; */
    
   

    entity_set_position(&npc2, 0, 200);
    /* npc2.position.x = 0;
     * npc2.position.y = 200;
     * npc2.colBox.x = 0;
     * npc2.colBox.y = 200; */

    entity_set_position(&player, 0, 0);
    /* player.position.x = 0;
     * player.position.y = 0;
     * player.colBox.x = 0;
     * player.colBox.y = 0; */
   
    camera_create ();
    camera_set_position_coord ((player.position.x + (player.current_frame.region_width / 2)),
                               (player.position.y + (player.current_frame.region_height / 2)));


    entity_move_left(&npc);
    glfwSetKeyCallback (get_game_window (), playstate_handle_keyboard);

    //create_texture("data/sprites/male_spritesheet.png", &entityTex);
}



void
playstate_handle_keyboard (GLFWwindow * window, int key, int scancode, int action, int mods)
{
    /* Key Pressed */
    if (action == GLFW_PRESS )
        {
            if (key == GLFW_KEY_ESCAPE)
                {
                    glfwSetWindowShouldClose (window, GL_TRUE);
                }
            else
                {
    
                    if (key == 'X')
                        {
                            entity_execute_attack (&player);
                        }
                    if (!player.attacking)
                        {
                            switch (key)
                                {
                                case GLFW_KEY_LEFT:
                                    entity_move_left(&player);
                                    break;
                                case GLFW_KEY_RIGHT:
                                    entity_move_right (&player);
                                    break;
                                case GLFW_KEY_UP:
                                    entity_move_up (&player);
                                    break;
                                case GLFW_KEY_DOWN:
                                    entity_move_down (&player);
                                    break;
                                }
    
                        }
                }
        }
    /* Key Released */
    else if (action == GLFW_RELEASE)
        {
            switch (key)
                {
                case GLFW_KEY_LEFT:
                    
                    entity_stop_move_left (&player);
                    break;
                case GLFW_KEY_RIGHT:
                    entity_stop_move_right (&player);
                    break;
                case GLFW_KEY_UP:
                    entity_stop_move_up (&player);
                    break;
                case GLFW_KEY_DOWN:
                    entity_stop_move_down (&player);
                    break;
                }
    
        }
}

static float rot = 0;

void
playstate_update ()
{
    log_message(INFO, "Updating...\n");
    ListElem *current_element = entities_list.head;

    while (current_element != NULL)
        {
            update_entity (current_element->data);

            /* Get next element in the list */
            current_element = current_element->next;

        }

    /* Check for collisions */
    ListElem *element_a = NULL;
    ListElem *element_b = NULL;

    int list_size = list_size(&entities_list);
    int i=0, j=0;
    
    element_a = list_head(&entities_list);
    element_b = list_head(&entities_list);
    
    while(element_a != NULL)
        {
            while(element_b != NULL)
                {
                    if(i != j) { 
                        Entity* eA = (Entity*)(element_a->data);
                        Entity* eB = (Entity*)(element_b->data);

                         bool colision = collide( &eA->colBox, &eB->colBox );
                         if(colision == true) {
                             entity_collided(eA);
                             log_message(INFO, "Entity speed (%f, %f)\n", eA->speed.x, eA->speed.y);
                         }
                        
                    } 
                    j++;
                    element_b = list_next(element_b);
                }

            i++;
            j=0;
            element_a = list_next(element_a);
            element_b = list_head(&entities_list);
        }
}








void
playstate_render (double interpolation)
{
    log_message(INFO, "Rendering...\n");
    Color colorRed;

    sprite_batch_begin ();



    rxsort_entities (&entities_list);

    ListElem *current_element = list_tail (&entities_list);
    int flag = 0;
    while (current_element != NULL)
        {
            if(flag == 0)
                {

                    colorRed.r = 1;
                    colorRed.g = 0.5f;
                    colorRed.b = 0.5f;
                    colorRed.a = 1;
                        
                        
                    sprite_batch_set_render_color(&colorRed);
                    
                    EntityPtr entity = (EntityPtr)(current_element->data);
                    
                    

                    entity->rotation += 0.5f;
                    
                    flag=1;
                }
            else
                {
                    colorRed.r = 1;
                    colorRed.g = 1;
                    colorRed.b = 1;
                    colorRed.a = 1;
                        
                        
                    sprite_batch_set_render_color(&colorRed);

                
                }

            render_entity (interpolation, current_element->data);
            current_element = current_element->previous;
        }

   
    camera_follow_entity(&player, interpolation);
    sprite_batch_end ();
}

void
playstate_finalize ()
{
    camera_destroy ();
    
    log_message (INFO, "Finalizing Play state\n");
}


void
entity_custom_b(EntityPtr entity) {
    entity_on_move(entity);

    entity->collided = false;

    TextureRegionPtr p_tex = get_next_frame (entity_get_current_animation(entity));
    entity->current_frame = *p_tex;

    if(entity->speed.x != 0 ||entity->speed.y != 0)
        {
            float posx = entity->position.x + entity->speed.x;
            float posy = entity->position.y + entity->speed.y;
            entity_set_position(entity, posx, posy);
            /* entity->position.x += entity->speed.x;
             * entity->position.y += entity->speed.y;
             * 
             * entity->colBox.x = entity->position.x;
             * entity->colBox.y = entity->position.y; */

            
        }
}
