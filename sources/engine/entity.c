#include "entity.h"
#include "graphics.h"
#include "spritebatch.h"
#include "logging.h"

#include <stdbool.h>
#include <string.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <math.h>

/* static void setup_entity_frames (Entity * entity); */




/* static void entity_on_move(Entity *entity); */

static void get_current_static_frame (Entity * entity, TextureRegion * current_frame);

static void parse_animation_info(xmlDocPtr doc, xmlNodePtr cur, Entity *entity);

static void parse_sprite_info(xmlDocPtr doc, xmlNodePtr cur, Entity *entity);


void entity_set_position(EntityPtr entity, float x, float y)
{
    if(entity == NULL)
        {
            log_message(ERROR, "entity_set_position(). NULL error. Entity can't be null");
            return;
        }

    float x_offset = abs(entity->position.x - entity->colBox.x);
    float y_offset = abs(entity->position.y - entity->colBox.y);
    
    
        
    
    entity->position.x = x;
    entity->position.y = y;
    entity->colBox.x = x + x_offset;
    entity->colBox.y = y + y_offset;

    
}


void
create_entity (char *name, Entity * entity)
{
    entity->name = name;
    create_texture ("sprites/male_spritesheet.png", &entity->texture);
    entity->position.x = 0;
    entity->position.y = 0;
    entity->speed.x = 0;
    entity->speed.y = 0;
    entity->accel.x = 0;
    entity->accel.y = 0;
    entity->direction = DOWN;
    entity->number_of_animations = 8;
    /* setup_entity_frames (entity); */
    entity->walking_up = false;
    entity->walking_down = false;
    entity->walking_right = false;
    entity->walking_left = false;
    entity->standing_up = false;
    entity->standing_down = false;
    entity->stanting_right = false;
    entity->standing_left = false;
    entity->attacking = false;
}


void
load_entity_from_xml(char * xml_file, Entity *entity)
{
    xmlDocPtr doc;
    xmlNodePtr cur;

    log_message(INFO, "Loading %s, parsing file...\n", xml_file);
    doc = xmlParseFile(xml_file);

    if(doc == NULL)
        {
            log_message(ERROR, "Document not parsed successfully.\n");
            fprintf(stderr, "Document not parsed successfully.\n");
            return ;
        }

    cur = xmlDocGetRootElement(doc);

    if(cur == NULL)
        {
            log_message(ERROR, "Empty document.\n");

            xmlFreeDoc(doc);
            return;
        }

    if(xmlStrcmp(cur->name, (const xmlChar *) "entity"))
        {
            log_message(ERROR, "Document of the wron type. Root node != 'entity'.\n");

            return;
        }

    cur = cur->xmlChildrenNode;
    while(cur != NULL) 
        {
            if( (!xmlStrcmp(cur->name, (const xmlChar *)"sprites")))
                {
                    parse_sprite_info(doc, cur, entity);
                }
            else if((!xmlStrcmp(cur->name, (const xmlChar *)"animations")))
                {
                    parse_animation_info(doc, cur, entity);
                }

            cur = cur->next;
        }
    
    entity->current_animation = standing_down;

    entity->rotation = 0;
    entity->position.x = 0;
    entity->position.y = 0;
    entity->speed.x = 0;
    entity->speed.y = 0;
    entity->accel.x = 0;
    entity->accel.y = 0;
    entity->direction = DOWN;
    entity->number_of_animations = 8;
    entity->walking_up = false;
    entity->walking_down = false;
    entity->walking_right = false;
    entity->walking_left = false;
    entity->standing_up = false;
    entity->standing_down = false;
    entity->stanting_right = false;
    entity->standing_left = false;
    entity->attacking = false;
    entity->colBox.x = entity->position.x;
    entity->colBox.y = entity->position.y;
    entity->colBox.width = 25;
    entity->colBox.height = 20;
    
    
}

void
parse_animation_info(xmlDocPtr doc, xmlNodePtr cur, Entity *entity) 
{
    
    
    xmlChar *name, *id, *max_frames, *frame_duration, *sprite_id, *start_x, *start_y, *frame_width, *frame_height, *anim_type;

    /* Before reading the child elements, we get the total number of them */
    int number_of_animations = atoi(xmlGetProp(cur, "total"));

    entity->number_of_animations = number_of_animations;
    /* We advance in the tree */
    cur = cur->xmlChildrenNode;



    entity->animations = malloc (number_of_animations * sizeof (Animation));
    int index = 0;
    while(cur != NULL)
        {
            if( (!xmlStrcmp(cur->name, (const xmlChar *)"anim")))
                {
                    name = xmlGetProp(cur, "name");
                    id = xmlGetProp(cur, "id");
                    max_frames = xmlGetProp(cur, "max-frames");
                    frame_duration = xmlGetProp(cur, "frame-duration");
                    sprite_id = xmlGetProp(cur, "sprite-id");
                    start_x = xmlGetProp(cur, "start-x");
                    start_y = xmlGetProp(cur, "start-y");
                    frame_width = xmlGetProp(cur, "frame-width");
                    frame_height = xmlGetProp(cur, "frame-height");
                    anim_type = xmlGetProp(cur, "anim-type");

                    setup_animation_f (atoi(max_frames), atof(frame_duration) , &(entity->texture), atoi(start_x), atoi(start_y), atoi(frame_width), atoi(frame_height), &entity->animations[index], atoi(anim_type));
                    
                    index++; 



                    xmlFree(name);
                    xmlFree(id);
                    xmlFree(max_frames);
                    xmlFree(frame_duration);
                    xmlFree(sprite_id);
                    xmlFree(start_x);
                    xmlFree(start_y);
                    xmlFree(frame_width);
                    xmlFree(frame_height);
                    
                }

            cur = cur->next;
        }
}

void
parse_sprite_info(xmlDocPtr doc, xmlNodePtr cur, Entity *entity)
{
    
    xmlChar *uri, *key;
    cur = cur->xmlChildrenNode;

   

    while(cur != NULL)
        {
            if( (!xmlStrcmp(cur->name, (const xmlChar *)"sprite")))
                {
                    uri = xmlGetProp(cur, "name");
                    
                    key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
                    
                    create_texture (key, &entity->texture);
                    xmlFree(uri);
                    xmlFree(key);
                  
                }
            cur = cur->next;

        }

    return;
        
}




bool
is_entity_moving (Entity * entity)
{
    bool is_moving = false;

    if (entity->walking_up == true || entity->walking_down == true || entity->walking_left == true
        || entity->walking_right == true)
        {
            is_moving = true;
        }

    return is_moving;
}


bool
is_entity_attacking (Entity * entity)
{
    return entity->attacking;
}


void
entity_set_current_animation(EntityPtr entity, size_t anim_id )
{
    
    if(anim_id > entity->number_of_animations)
        {
            log_message(ERROR, "entity_set_current_animation: '%d', Index out of bounds.", anim_id);
            return;
        }
    
    entity->current_animation = anim_id;
}

Animation* entity_get_current_animation(EntityPtr entity)
{
    return &entity->animations[entity->current_animation];
}


void
update_entity (Entity * entity)
{

    TextureRegion *p_tex;       

    /* if (entity->attacking)
     *     {
     *         if (entity->current_animation.animation_finished)
     *             {
     *                 entity->attacking = false;
     *                 entity_fix_direction (entity);
     *                 return;
     *             }
     *         else
     *             {
     *                 p_tex = get_next_frame (&entity->current_animation, false);
     *                 entity->current_frame = *p_tex;
     *             }
     *     }
     * else if (is_entity_moving (entity))
     *     { */

    if(entity->custom_behaviour != NULL)
        {
            entity->custom_behaviour(entity);
        }


    
    /* entity_on_move(entity);
     * 
     * entity->collided = false;
     * 
     * p_tex = get_next_frame (entity_get_current_animation(entity));
     * entity->current_frame = *p_tex;
     * 
     * if(entity->speed.x != 0 ||entity->speed.y != 0)
     *     {
     *         float posx = entity->position.x + entity->speed.x;
     *         float posy = entity->position.y + entity->speed.y;
     *         entity_set_position(entity, posx, posy);
     *         /\* entity->position.x += entity->speed.x;
     *          * entity->position.y += entity->speed.y;
     *          * 
     *          * entity->colBox.x = entity->position.x;
     *          * entity->colBox.y = entity->position.y; *\/
     * 
     *         
     *     } */


    /* if (entity->position.x < 0)
     *     {
     *         entity->position.x = 0;
     *     }
     * if (entity->position.y < 0)
     *     {
     *         entity->position.y = 0;
     *     } */
    /*     }
     * else
     *     {
     *         get_current_static_frame (entity, &entity->current_frame);
     *         entity->speed.x = 0;
     *         entity->speed.y = 0;
     * 
     *       
     *     } */
}


void
entity_collided(Entity * entity)
{
   

    if(entity->speed.x != 0 || entity->speed.y != 0)
        {
            
            log_message(INFO, "Moving entity back to original pos\n");
            
            float posx = entity->position.x;
            float posy = entity->position.y;
            
            if(entity->speed.x != 0)
                posx -= entity->speed.x;
            
            if(entity->speed.y != 0)
                posy -= entity->speed.y;

            entity_set_position(entity, posx, posy);

            entity->collided = true;
            
            /* if(entity->walking_up_left)
             *     entity_stop_move_up_left(entity);
             * else if(entity->walking_down_left)
             *     entity_stop_move_down_left(entity);
             * else if(entity->walking_up_right)
             *     entity_stop_move_up_right(entity);
             * else if(entity->walking_down_right)
             *     entity_stop_move_down_right(entity);
             * else if(entity->walking_right)
             *     entity_stop_move_right(entity);
             * else if(entity->walking_left)
             *     entity_stop_move_left(entity);
             * else if(entity->walking_up)
             *     entity_stop_move_up(entity);
             * else if(entity->walking_down)
             *     entity_stop_move_down(entity); */
            
           

            /* if(entity->speed.x != 0)
             *     {
             *         if(entity->speed.x > 0) 
             *             entity_stop_move_right(entity);
             *         else
             *             entity_stop_move_left(entity);
             *         
             *         
             *     }
             * 
             * if(entity->speed.y != 0)
             *     {
             *         if(entity->speed.y > 0)
             *             entity_stop_move_up(entity);
             *         else
             *             entity_stop_move_down(entity);
             * 
             *         
             *     } */
        
        }

}


void
render_entity (double interpolation, Entity * entity)
{
     log_message(INFO, "Entity speed on Render (%f, %f)\n", entity->speed.x, entity->speed.y);
 
    double draw_x = entity->position.x;
    double draw_y = entity->position.y;
    
    if(entity->collided == false)
        {

            draw_x += (entity->speed.x* interpolation);
            draw_y += (entity->speed.y* interpolation);
        }

    sprite_batch_draw_region_r (&entity->current_frame, draw_x, draw_y, entity->rotation);
}

void
get_current_static_frame (Entity * entity, TextureRegion * current_frame)
{

    //TextureRegion *region = malloc ( sizeof ( TextureRegion ) );
    switch (entity->direction)
        {
        case UP:
            *current_frame = create_texture_region (&(entity->texture), 0, 0, 64, 64);
            break;
        case LEFT:
            *current_frame = create_texture_region (&(entity->texture), 0, 64, 64, 64);
            break;
        case DOWN:
            *current_frame = create_texture_region (&(entity->texture), 0, 128, 64, 64);
            break;
        case RIGHT:
            *current_frame = create_texture_region (&(entity->texture), 0, 192, 64, 64);
            break;
        default:
            log_message(ERROR, "Could not detect entity state\n");

            current_frame = NULL;
            break;
        }

    //current_frame = region;
}



void
entity_execute_attack (Entity * entity)
{
    entity->attacking = true;
    switch (entity->direction)

        {
        case UP:
            entity->current_animation = attack_up;
            break;
        case DOWN:
            entity->current_animation = attack_down;
            break;
        case LEFT:
            entity->current_animation = attack_left;
            break;
        case RIGHT:
            entity->current_animation = attack_right;
            break;
        }

    reset_animation ( entity_get_current_animation(entity) );
}


/* void 
 * entity_move(Entity * entity, int direction)
 * {
 *     switch (direction) {
 *     case UP:
 *         entity->walking_up = true;
 *         entity->walking_down = false;
 *         entity->walking_left = false;
 *         entity->walking_right = false;
 *         break;
 *     case DOWN:
 *         entity->walking_up = false;
 *         entity->walking_down = true;
 *         entity->walking_left = false;
 *         entity->walking_right = false;
 *         break;
 *     case LEFT:
 *         entity->walking_up = false;
 *         entity->walking_down = false;
 *         entity->walking_left = true;
 *         entity->walking_right = false;
 *         break;
 *     case RIGHT:
 *         entity->walking_up = false;
 *         entity->walking_down = false;
 *         entity->walking_left = false;
 *         entity->walking_right = true;
 *         break;
 * 
 *     }
 * } */


void 
entity_stop_move(Entity * entity, int direction)
{
    switch (direction) {
    case UP:
        entity->walking_up = false;
        break;
    case DOWN:
        entity->walking_down = false;
        break;
    case LEFT:
        entity->walking_left = false;
        break;
    case RIGHT:
        entity->walking_right = false;
        break;

    }
}


void
entity_on_move(EntityPtr entity)
{
    if(entity->walking_up)
        {
            entity->speed.y = 4;
            entity->speed.x = 0;
            if(entity->current_animation != walk_up)
                entity->current_animation = walk_up;
            entity->direction = UP;
        }
    else if(entity->walking_down)
        {
            entity->speed.y = -4;
            entity->speed.x = 0;
            if(entity->current_animation != walk_down)
                entity->current_animation = walk_down;
            entity->direction = DOWN;
        }
    else if(entity->walking_right)
        {
            entity->speed.y = 0;
            entity->speed.x = 4;
            if(entity->current_animation != walk_right)
                entity->current_animation = walk_right;
            entity->direction = RIGHT;
        }
    else if(entity->walking_left)
        {
            entity->speed.y = 0;
            entity->speed.x = -4;
            if(entity->current_animation != walk_left)
                entity->current_animation = walk_left;
            entity->direction = LEFT;
        }
    else if(entity->walking_up_left)
        {
            entity->speed.x = -4.0f/ 1.75f;
            entity->speed.y = 4.0f/ 1.75f;
            if(entity->current_animation != walk_left)
            entity->current_animation = walk_left;
            entity->direction = UPLEFT;
        }
    else if(entity->walking_up_right)
        {
            entity->speed.x = 4.0f/ 1.75f;
            entity->speed.y = 4.0f/ 1.75f;
            if(entity->current_animation != walk_right)
                entity->current_animation = walk_right;
            entity->direction = UPRIGHT;
        }
    else if(entity->walking_down_left)
        {
            entity->speed.x = -4.0f/ 1.75f;
            entity->speed.y = -4.0f/ 1.75f;
            if(entity->current_animation != walk_left)
                entity->current_animation = walk_left;
            entity->direction = DOWNLEFT;
        }
    else if(entity->walking_down_right)
        {
            entity->speed.x = 4.0f / 1.75f;
            entity->speed.y = -4.0f/ 1.75f;
            if(entity->current_animation != walk_right)
                entity->current_animation = walk_right;
            entity->direction = DOWNRIGHT;
        }
    else {
        entity_stop(entity);
    }
}



void
entity_stop(Entity *entity)
{
    entity->speed.x = 0;
    entity->speed.y = 0;

    entity->walking_up = false;
    entity->walking_left = false;
    entity->walking_down = false;
    entity->walking_right = false;
    entity->walking_up_left = false;
    entity->walking_up_right = false;
    entity->walking_down_left = false;
    entity->walking_down_right = false;

    if(entity->direction == DOWN)
        {
            entity->current_animation = standing_down;
        }
    else if(entity->direction == UP)
        {
            entity->current_animation = standing_up;
        }
    else if(entity->direction == LEFT)
        {
            entity->current_animation = standing_left;
        }
    else if(entity->direction == RIGHT)
        {
            entity->current_animation = standing_right;
        }
    else if(entity->direction == UPLEFT)
        {
            entity->current_animation = standing_left;
        }
    else if(entity->direction == UPRIGHT)
        {
            entity->current_animation = standing_right;
        }
    else if(entity->direction == DOWNLEFT)
        {
            entity->current_animation = standing_left;
        }
    else if(entity->direction == DOWNRIGHT)
        {
            entity->current_animation = standing_right;
        }
    
     
}




void
entity_move_down (Entity * entity)
{
   
    if(!entity->walking_down && !entity->walking_down_right && !entity->walking_down_left)
        {
            if(entity->walking_left)
                {
                    entity->walking_left = false;
                    entity->walking_down_left = true;
                }
            else if (entity->walking_right)
                {
                    entity->walking_right = false;
                    entity->walking_down_right = true;
                }
            else {
                entity->walking_down = true;
            }
        }

}




void
entity_move_up (Entity * entity)
{

    if(!entity->walking_up && !entity->walking_up_right && !entity->walking_up_left)
        {
            if(entity->walking_left)
                {
                    entity->walking_left = false;
                    entity->walking_up_left = true;
                }
            else if (entity->walking_right)
                {
                    entity->walking_right = false;
                    entity->walking_up_right = true;
                }
            else {
                entity->walking_up = true;
            }
        }


}




void
entity_move_left (Entity * entity)
{
    
    if(entity->walking_left || entity->walking_up_left || entity->walking_down_left)
        {}
    else
        {
            if(entity->walking_down)
                {
                    entity->walking_down = false;
                    entity->walking_down_left = true;
                }
            else if(entity->walking_up)
                {
                    entity->walking_up = false;
                    entity->walking_up_left = true;
                }
            else {
                entity->walking_left = true;
            } 
        }


}




void
entity_move_right (Entity * entity)
{
    if(!entity->walking_right && !entity->walking_up_right && !entity->walking_down_right)
        {
            if(entity->walking_down)
                {
                    entity->walking_down = false;
                    entity->walking_down_right = true;
                }
            else if(entity->walking_up)
                {
                    entity->walking_up = false;
                    entity->walking_up_right = true;
                }
            else {
                entity->walking_right = true;
            } 
        }


}



void
entity_stop_move_up (Entity * entity)
{
   
    
    if(entity->walking_up_left)
        {
            entity->walking_up_left = false;
            entity->walking_left = true;
        }
    else if(entity->walking_up_right)
        {
            entity->walking_up_right = false;
            entity->walking_right = true;
        }
    entity->walking_up = false;


}


void 
entity_stop_move_up_right(Entity *entity)
{
    entity->walking_up_right = false;
    
    if(entity->walking_up)
        {
            entity->walking_up = true;
        }
    if(entity->walking_right)
        {
            entity->walking_right = true;
        }

}


void 
entity_stop_move_up_left(Entity *entity)
{
    entity->walking_up_left = false;
    
    if(entity->walking_up)
        {
            entity->walking_up = true;
        }
    if(entity->walking_left)
        {
            entity->walking_left = true;
        }

}


void 
entity_stop_move_down_right(Entity *entity)
{
    entity->walking_down_right = false;
    
    if(entity->walking_down)
        {
            entity->walking_down = true;
        }
    if(entity->walking_right)
        {
            entity->walking_right = true;
        }

}

void 
entity_stop_move_down_left(Entity *entity)
{
    entity->walking_down_left = false;
    
    if(entity->walking_down)
        {
            entity->walking_down = true;
        }
    if(entity->walking_left)
        {
            entity->walking_left = true;
        }

}


void
entity_stop_move_down (Entity * entity)
{
    
    if(entity->walking_down_left)
        {
            entity->walking_down_left = false;
            entity->walking_left = true;
        }
    else if(entity->walking_down_right)
        {
            entity->walking_down_right = false;
            entity->walking_right = true;
        }
    entity->walking_down = false;



}



void
entity_stop_move_left (Entity * entity)
{

    if(entity->walking_up_left)
        {
            entity->walking_up_left = false;
            entity->walking_up = true;
        }
    else if(entity->walking_down_left)
        {
            entity->walking_down_left = false;
            entity->walking_down = true;
        }

    entity->walking_left = false;


}


void
entity_stop_move_right (Entity * entity)
{
    

    if(entity->walking_up_right)
        {
            entity->walking_up_right = false;
            entity->walking_up = true;
        }
    else if(entity->walking_down_right)
        {
            entity->walking_down_right = false;
            entity->walking_down = true;
        }
    entity->walking_right = false;


}




void
entity_fix_direction (Entity * entity)
{
    if (!entity->attacking)

        {
            if (entity->walking_down)
                {
                    entity->direction = DOWN;
                    entity->current_animation = walk_down;
                }
            if (entity->walking_up)

                {
                    entity->direction = UP;
                    entity->current_animation =walk_up;
                }
            if (entity->walking_right)

                {
                    entity->direction = RIGHT;
                    entity->current_animation = walk_right;
                }
            if (entity->walking_left)

                {
                    entity->direction = LEFT;
                    entity->current_animation = walk_left;
                }
        }
}

