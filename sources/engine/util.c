#include "util.h"
#include "logging.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Boring, non-OpenGL-related utility functions
 */
void *
file_contents (file_type type, const char *filename, GLint * length)
{
    char *file_path = NULL;
    char *final_path = NULL;

    switch (type)
        {
        case IMG:
            file_path = "data/img/";
            break;
        case SHD:
            file_path = "data/shaders/";
            break;
        case MAP:
            file_path = "data/maps/";
            break;
        case DAT:
            file_path = "data/definitions/";
            break;
        case SAV:
            file_path = "data/saves/";
            break;
        default:
            file_path = "data/";
            break;
        }

    final_path = malloc (strlen (file_path) + strlen (filename) + 1);
    strcpy (final_path, file_path);
    strcat (final_path, filename);
    FILE *f = fopen (final_path, "r");
    void *buffer;

    if (!f)
        {
            log_message(ERROR,"Unable to open %s for reading\n", filename);
         
            return NULL;
        }

    fseek (f, 0, SEEK_END);
    *length = ftell (f);
    fseek (f, 0, SEEK_SET);
    buffer = malloc (*length + 1);
    *length = fread (buffer, 1, *length, f);
    fclose (f);
    ((char *)buffer)[*length] = '\0';
    return buffer;
}


static short
le_short (unsigned char *bytes)
{
    return bytes[0] | ((char)bytes[1] << 8);
}


void *
read_tga (const char *filename, int *width, int *height)
{
    char *file_path = "data/img/";
    char *final_path = malloc (strlen (file_path) + strlen (filename) + 1);

    strcpy (final_path, file_path);
    strcat (final_path, filename);
    struct tga_header
    {
        char id_length;
        char color_map_type;
        char data_type_code;
        unsigned char color_map_origin[2];
        unsigned char color_map_length[2];
        char color_map_depth;
        unsigned char x_origin[2];
        unsigned char y_origin[2];
        unsigned char width[2];
        unsigned char height[2];
        char bits_per_pixel;
        char image_descriptor;
    } header;

    int i, color_map_size, pixels_size;

    FILE *f;
    size_t read;
    void *pixels;

    f = fopen (final_path, "rb");

    if (!f)
        {
            log_message(ERROR,"Unable to open %s for reading\n", filename);

            return NULL;
        }

    read = fread (&header, 1, sizeof (header), f);

    if (read != sizeof (header))
        {
            log_message(ERROR,"%s has incomplete tga header\n", filename);
            fclose (f);
            return NULL;
        }
    if (header.data_type_code != 2)
        {
            log_message (ERROR, "%s is not an uncompressed RGB tga file\n", filename);
            fclose (f);
            return NULL;
        }
    if (header.bits_per_pixel != 24)
        {
            log_message (ERROR, "%s is not a 24-bit uncompressed RGB tga file\n", filename);
            fclose (f);
            return NULL;
        }

    for (i = 0; i < header.id_length; ++i)
        if (getc (f) == EOF)
            {
                log_message (ERROR, "%s has incomplete id string\n", filename);
                fclose (f);
                return NULL;
            }
    color_map_size = le_short (header.color_map_length) * (header.color_map_depth / 8);

    for (i = 0; i < color_map_size; ++i)
        if (getc (f) == EOF)
            {
                log_message (ERROR, "%s has incomplete color map\n", filename);
                fclose (f);
                return NULL;
            }
    *width = le_short (header.width);
    *height = le_short (header.height);
    pixels_size = *width * *height * (header.bits_per_pixel / 8);
    pixels = malloc (pixels_size);
    read = fread (pixels, 1, pixels_size, f);

    if (read != pixels_size)
        {
            log_message (ERROR, "%s has incomplete image\n", filename);
            fclose (f);
            free (pixels);
            return NULL;
        }
    return pixels;
}
