#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "entity.h"
#include "rsort.h"
#include "list.h"

#define TURNAROUND 57535


static int max_y_pos (List * entities_list);
static int min_y_pos (List * entities_list);
static int max_number (int *data_array, int size);
static int get_number_of_digits (int number);



int
rxsort (int *data, int size)
{

    int *counts;

    /* variable to store the sorted elements */
    int *temp;

    /* radix will always be 10 as we are using nmbers from 0-9 */
    int radix = 10;


    /* Number of digits of the max number in the array */
    int number_of_digits = 0;


    int index;
    int pval;

    int i;
    int j;
    int n;

    int start_time = clock ();


    /* Find the max number in the array */
    int max_num = max_number (data, size);

    /* Sets the number of digits to use in the sorting method */
    number_of_digits = get_number_of_digits (max_num);


    /* Allocate storage for counts */
    if ((counts = (int *)malloc (radix * sizeof (int))) == NULL)
        {
            return -1;
        }

    /* Allocate storage for sorted elements */
    if ((temp = (int *)malloc (size * sizeof (int))) == NULL)
        {
            return -1;
        }

    /* Sot from the least significant position to the most significant */
    for (n = 0; n < number_of_digits; n++)
        {

            /* initialize the counts */
            for (i = 0; i < radix; i++)
                {
                    counts[i] = 0;
                }

            /* calculate the position value */
            pval = (int)pow ((double)radix, (double)n);


            /*count the occurrences of each digit value */
            for (j = 0; j < size; j++)
                {
                    index = (int)(data[j] / pval) % radix;
                    counts[index] = counts[index] + 1;
                }


            /* Adjust each count to reflect the counts before it */
            for (i = 1; i < radix; i++)
                {
                    counts[i] = counts[i] + counts[i - 1];
                }

            /* use the counts to position each element where it belongs */
            for (j = size - 1; j >= 0; j--)
                {
                    index = (int)(data[j] / pval) % radix;
                    temp[counts[index] - 1] = data[j];
                    counts[index] = counts[index] - 1;
                }

            /* prepare to pass back the data as sorted thus far */
            memcpy (data, temp, size * sizeof (int));

        }

    free (counts);
    free (temp);

    int end_time = clock ();
    float diff = (((float)end_time - (float)start_time) / 1000000.0F) * 1000;

    

    return 0;
}



/**
 * radix sort implemented for Entities
 */

int
rxsort_entities (List * data)
{

    if (data == NULL)
        return -1;

    clock_t start_time = clock ();

    int size = list_size (data);

    int *counts;

    /* variable to store the sorted elements */
    ListElem *temp[size];

    /* radix will always be 10 as we are using nmbers from 0-9 */
    int radix = 10;


    /* Number of digits of the max number in the array */
    int number_of_digits = 0;


    int index;
    int pval;

    int i;
    int j;
    int n;

    ListElem *current_element = NULL;
    ListElem *previous_element = NULL;

    float min_number = 0;

    min_number = abs (min_y_pos (data));

    current_element = list_head (data);
    while (current_element != NULL)
        {
            Entity *entity = (Entity *) list_data (current_element);

            entity->position.y += min_number;

            current_element = current_element->next;
        }

    /* Find the max number in the array */
    int max_num = max_y_pos (data);

    /* Sets the number of digits to use in the sorting method */
    number_of_digits = get_number_of_digits (max_num);


    /* Allocate storage for counts */
    if ((counts = (int *)malloc (radix * sizeof (int))) == NULL)
        {
            return -1;
        }

    /* Allocate storage for sorted elements */
    /* for (i = 0; i < size; i++)
     *     {
     *         if ((temp[0] = (ListElem *) malloc (size * sizeof (ListElem))) == NULL)
     *             {
     *                 return -1;
     *             }
     * 
     *     } */



    /* Sort from the least significant position to the most significant */
    for (n = 0; n < number_of_digits; n++)
        {

            /* initialize the counts */
            for (i = 0; i < radix; i++)
                {
                    counts[i] = 0;
                }

            /* calculate the position value */
            pval = (int)pow ((double)radix, (double)n);


            /*count the occurrences of each digit value */
            current_element = list_head (data);
            while (current_element != NULL)
                {
                    Entity *curr_entity = list_data (current_element);

                    index = (int)(curr_entity->position.y / pval) % radix;
                    counts[index] = counts[index] + 1;

                    current_element = list_next (current_element);
                }

            /* Adjust each count to reflect the counts before it */
            for (i = 1; i < radix; i++)
                {
                    counts[i] = counts[i] + counts[i - 1];
                }

            /* use the counts to position each element where it belongs */
            current_element = list_tail (data);
            while (current_element != NULL)
                {
                    Entity *curr_entity = list_data (current_element);

                    index = (int)(curr_entity->position.y / pval) % radix;
                    temp[counts[index] - 1] = current_element;
                    counts[index] = counts[index] - 1;

                    current_element = list_previous (current_element);
                }

            /* prepare to pass back the data as sorted thus far */
            int l;


            data->head = temp[0];
            data->tail = temp[size - 1];

            data->head->previous = NULL;
            data->tail->next = NULL;

            /* data->head->next = list_tail(data);
             * data->tail->previous = list_head(data); */

            current_element = list_head (data);
            /* current_element = list_head (data);
             * current_element->previous = NULL;
             * previous_element = NULL; */

            for (l = 1; l < size - 1; l++)
                {
                    current_element->next = temp[l];
                    current_element->next->previous = current_element;
                    current_element = current_element->next;
                }

            current_element->next = list_tail (data);
            current_element->next->previous = current_element;
            current_element = list_tail (data);

        }

    /* Return highest bit to normal */
    current_element = list_head (data);
    while (current_element != NULL)
        {
            Entity *entity = (Entity *) list_data (current_element);

            entity->position.y -= min_number;


            current_element = current_element->next;
        }


    free (counts);

    clock_t end_time = clock ();
    double diff = (((float)end_time - (float)start_time) / 1000000.0F) * 1000;



    return 0;
}


int
get_number_of_digits (int number)
{
    int count = 0;

    while (number)
        {
            number = number / 10;
            count++;
        }

  
    return count;
}


int
max_number (int *data_array, int size)
{
    int max_value = 0;
    int i;

    for (i = 0; i < size; i++)
        {
            if (max_value < data_array[i])
                {
                    max_value = data_array[i];
                }
        }



    return max_value;
}


int
max_y_pos (List * entities_list)
{
    int max_value = 0;

    ListElem *current = (ListElem *) (entities_list->tail);

    while (current != NULL)
        {
            Entity *currentEntity = (Entity *) (current->data);

            if (currentEntity->position.y > max_value)
                {
                    max_value = (int)(currentEntity->position.y);
                }
            current = current->previous;
        }

    return max_value;
}

int
min_y_pos (List * entities_list)
{
    int max_value = 0;

    ListElem *current = (ListElem *) (entities_list->head);

    while (current != NULL)
        {
            Entity *currentEntity = (Entity *) (current->data);

            if (currentEntity->position.y < max_value)
                {
                    max_value = (int)(currentEntity->position.y);
                }
            current = current->next;
        }

    return max_value;
}
