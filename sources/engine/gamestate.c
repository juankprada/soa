#include "gamestate.h"
void
soa_copy_game_state (GameState * dest, GameState * source)
{
    dest->on_init = source->on_init;
    dest->on_update = source->on_update;
    dest->on_render = source->on_render;
    dest->on_finish = source->on_finish;
}
