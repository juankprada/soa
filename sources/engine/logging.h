#ifndef _LOGGING_H_
#define _LOGGING_H_


#ifndef LOG_LEVEL
#define LOG_LEVEL 2
#endif

typedef enum LogType LogType;

enum LogType 
    {
        INFO, WARNING, ERROR, DEBUG
    };

extern void log_message(LogType level, const char* message, ...);

#endif
