

/*
 * glshader.c
 *
 *  Created on: Aug 19, 2012
 *      Author: juankprada
 */
#include <stdio.h>
#include <stdlib.h>
#include "glshader.h"
#include "logging.h"


void
show_info_log (GLuint object, PFNGLGETSHADERIVPROC glGet__iv, PFNGLGETSHADERINFOLOGPROC glGet__InfoLog)
{
    GLint log_length;
    char *log;


    glGet__iv (object, GL_INFO_LOG_LENGTH, &log_length);
    log = malloc (log_length);

    glGet__InfoLog (object, log_length, NULL, log);
    log_message(ERROR, "%s", log);
    free (log);
}

GLuint
make_shader (GLenum type, const char *filename)
{
    GLint length;
    GLchar *source = file_contents (SHD, filename, &length);
    GLuint shader;
    GLint shader_ok;

    if (!source)
        return 0;
    shader = glCreateShader (type);
    glShaderSource (shader, 1, (const GLchar **)&source, &length);
    free (source);
    glCompileShader (shader);
    glGetShaderiv (shader, GL_COMPILE_STATUS, &shader_ok);

    if (!shader_ok)
        {
            log_message(ERROR, "Failed to compile %s:\n", filename);
            show_info_log (shader, glGetShaderiv, glGetShaderInfoLog);
            glDeleteShader (shader);
            return 0;
        }

    return shader;
}


GLuint
make_shader_program (GLuint vertex_shader, GLuint fragment_shader)
{
    GLint program_ok = 0;
    GLuint program = 0;

    program = glCreateProgram ();

    if (program == 0)
        {
            log_message(ERROR, "Failed to link shader program:\n");
            show_info_log (program, glGetProgramiv, glGetProgramInfoLog);
            return 0;
        }
    glAttachShader (program, vertex_shader);
    glAttachShader (program, fragment_shader);
    glLinkProgram (program);
    glGetProgramiv (program, GL_LINK_STATUS, &program_ok);

    if (!program_ok)
        {
            log_message(ERROR, "Failed to link shader program:\n");
            show_info_log (program, glGetProgramiv, glGetProgramInfoLog);
            glDeleteProgram (program);
            return 0;
        }

    return program;
}

CGLShader
setup_shader (const char *vertex_shader_name, const char *fragment_shader_name)
{
    CGLShader shader;

    /* Create a vertex shader */
    shader.vertex_shader = make_shader (GL_VERTEX_SHADER, vertex_shader_name);

    if (shader.vertex_shader == 0)
        {
            log_message (ERROR, "unable to create vertex shader\n");
            exit (-1);
        }

    /* create a fragment shader */
    shader.fragment_shader = make_shader (GL_FRAGMENT_SHADER, fragment_shader_name);

    if (shader.fragment_shader == 0)
        {
            log_message (ERROR, "unable to create fragment shader\n");

            exit (-1);
        }

    /* make program with previous created shaders */
    shader.program = make_shader_program (shader.vertex_shader, shader.fragment_shader);

    if (shader.program == 0)
        {
            log_message (ERROR, "unable to create shader program\n");
        }

    return shader;
}
