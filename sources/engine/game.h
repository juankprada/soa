
/*
 * game.h
 *
 *  Created on: Sep 16, 2012
 *      Author: Juankprada
 */

#ifndef GAME_H_
#define GAME_H_

#include <stdio.h>
#include <stdbool.h>

#include "gamestate.h"

//extern ALLEGRO_TIMER *GAME_TIMER;

void soa_get_window_size (int *w, int *h);

void soa_get_resolution (int *w, int *h);

void soa_create_game_window ();

void soa_close_game_window ();

void soa_run_game ();

void soa_set_game_state (GameState * state);

GLFWwindow *get_game_window ();


#endif /*  */
