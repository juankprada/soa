#ifndef GAMESTATE_H_INCLUDED
#define GAMESTATE_H_INCLUDED


#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include <opengl.h>

typedef struct GameState GameState;
typedef void (*ptrState_on_init) (void);
typedef void (*ptrState_on_update) ();
typedef void (*ptrState_on_render) (double interpolation);
typedef void (*ptrState_on_finish) ();

struct GameState
{
    ptrState_on_init on_init;
    ptrState_on_update on_update;
    ptrState_on_render on_render;
    ptrState_on_finish on_finish;
};

extern void soa_copy_game_state (GameState * dest, GameState * source);


#endif // GAMESTATE_H_INCLUDED
