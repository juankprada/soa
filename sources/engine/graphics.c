
/*
 * texture.c
 *
 *  Created on: Aug 27, 2012
 *      Author: juankprada
 */

#include "graphics.h"
#include "logging.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <png.h>


void
create_texture (const char *file, Texture * texture_struct)
{

    int outWidth, outHeight;
    bool outHasAlphax;
    GLubyte *outData;

    png_structp png_ptr;
    png_infop info_ptr;
    unsigned int sig_read = 0;
    int color_type, interlace_type;
    FILE *fp;

    texture_struct->height = 0;
    texture_struct->width = 0;
    texture_struct->texture_id = 0;

    char *file_path = "data/";
    char *final_path = malloc (strlen (file_path) + strlen (file) + 1);


    strcpy (final_path, file_path);
    strcat (final_path, file);

    if ((fp = fopen (final_path, "rb")) == NULL)
        {
log_message(ERROR, "Could not openg %s\n", file);

            exit (-1);
        }

    png_ptr = png_create_read_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (png_ptr == NULL)
        {
log_message(ERROR, "png_ptr is null\n");

            fclose (fp);
            exit (-1);
        }

    /* Allocate the memory for image information. REQUIRED */
    info_ptr = png_create_info_struct (png_ptr);
    if (info_ptr == NULL)
        {
            fclose (fp);
            png_destroy_read_struct (&png_ptr, NULL, NULL);
            fprintf (stderr, "info_ptr is null\n");
            exit (-1);
        }

    /* Set error handling if you are using the setjmp/longjmp method
     * (this is the normal method of doing things with libpng)
     * REQUIRED unless you set up your own error handlers in
     * the png_create_read_struct() earlier
     */
    if (setjmp (png_jmpbuf (png_ptr)))
        {
            png_destroy_read_struct (&png_ptr, &info_ptr, NULL);
            fclose (fp);
log_message(ERROR, "Error setting jmp");

            exit (-1);
        }

    /* set up the output control if you are using standard c streams */
    png_init_io (png_ptr, fp);

    /* if we have already read some of the signature */
    png_set_sig_bytes (png_ptr, sig_read);


    png_read_png (png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, NULL);

    outWidth = png_get_image_width (png_ptr, info_ptr);
    outHeight = png_get_image_height (png_ptr, info_ptr);

    switch (png_get_color_type (png_ptr, info_ptr))
        {
        case PNG_COLOR_TYPE_RGBA:
            outHasAlphax = true;
            break;
        case PNG_COLOR_TYPE_RGB:
            outHasAlphax = false;
            break;
        default:
log_message(ERROR, "Color type not supported\n");
            png_destroy_read_struct (&png_ptr, &info_ptr, NULL);
            fclose (fp);
            exit (-1);
        }

    unsigned int row_bytes = png_get_rowbytes (png_ptr, info_ptr);

    outData = (unsigned char *)malloc (row_bytes * outHeight);

    png_bytepp row_pointers = png_get_rows (png_ptr, info_ptr);

    int i;

    for (i = 0; i < outHeight; i++)
        {
            memcpy (outData + (row_bytes * (outHeight - 1 - i)), row_pointers[i], row_bytes);
        }


    /* clean up after read and free any memory allocated */
    png_destroy_read_struct (&png_ptr, &info_ptr, NULL);

    fclose (fp);
    glEnable (GL_BLEND);
    glGenTextures (1, &(texture_struct->texture_id));
    glBindTexture (GL_TEXTURE_2D, texture_struct->texture_id);

    //  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D (GL_TEXTURE_2D, 0, outHasAlphax ? GL_RGBA8 : GL_RGB8, outWidth, outHeight, 0,
                  outHasAlphax ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, outData);

    glDisable (GL_BLEND);
    texture_struct->width = outWidth;
    texture_struct->height = outHeight;

    free (final_path);
    free (outData);
}

TextureRegion
create_texture_region (Texture * texture, float x, float y, float width, float height)
{
    TextureRegion region;

    region.texture = texture;
    float y2 = (texture->height - y) - height;
    float invTexWidth = 1.0f / texture->width;
    float invTexHeight = 1.0f / texture->height;

    region.u = (x * invTexWidth);
    region.v = (y2 * invTexHeight);
    region.u2 = (x + width) * invTexWidth;
    region.v2 = (y2 + height) * invTexHeight;
    region.region_width = round (fabs ((float)(region.u2 - region.u)) * texture->width);
    region.region_height = round (fabs ((float)(region.v2 - region.v)) * texture->height);
    
    return region;
}
