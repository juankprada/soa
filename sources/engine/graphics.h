#ifndef _GRAPHICS_H_INCLUDE_
#define _GRAPHICS_H_INCLUDE_

#include "../include/opengl.h"
#include <stdio.h>
#include <stdlib.h>
#include <kazmath/kazmath.h>

#include "list.h"

typedef unsigned char *PNGImage;

typedef struct Texture Texture;

typedef Texture* TexturePtr;

typedef struct TextureRegion TextureRegion;

typedef TextureRegion* TextureRegionPtr;

typedef struct Sprite Sprite;

typedef Sprite* SpritePtr;



struct Texture
{
    GLuint texture_id;
    int width;
    int height;
};



struct TextureRegion
{
    TexturePtr texture;
    GLfloat u;
    GLfloat v;
    GLfloat u2;
    GLfloat v2;
    float region_width;
    float region_height;
};


struct Sprite
{
    List frames;

    float scale_x;
    float scale_y;
    
    float rotation;

    float translation_x;
    float translation_y;

    kmVec2 pos;
};


extern void create_texture (const char *file, Texture * texture_struct);
extern TextureRegion create_texture_region (Texture * texture, float x, float y, float width, float height);


#endif /*  */
