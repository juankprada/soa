#ifndef _UTIL_H_INCLUDE
#define _UTIL_H_INCLUDE

#include "../include/opengl.h"


typedef enum file_type
{ IMG, SHD, MAP, DAT, SAV } file_type;
extern void *file_contents (file_type type, const char *filename, GLint * length);
extern void *read_tga (const char *filename, int *width, int *height);


#endif
