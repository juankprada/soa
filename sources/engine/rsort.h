#ifndef _RSORT_H_
#define _RSORT_H_


#include "entity.h"
#include "list.h"

int rxsort (int *data, int size);
int rxsort_entities (List * data);

#endif /* _RSORT_H_ */
