#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED


#include <kazmath/kazmath.h>

#include "animation.h"
#include "graphics.h"
#include "collisions.h"


typedef struct Entity Entity;

typedef Entity* EntityPtr;

typedef enum FacingDirection FacingDirection;


typedef void (*ptrEntity_behaviour)(EntityPtr entity);
typedef void (*ptrEntity_collided)(EntityPtr entity);



enum FacingDirection
    {
        UP, LEFT, DOWN, RIGHT, UPLEFT, UPRIGHT, DOWNLEFT, DOWNRIGHT
    };


struct Entity
{
    float rotation;
    Texture texture;
    TextureRegion current_frame;

    int current_animation;
    Animation *animations;
    int number_of_animations;
    
    char *name;
    
    kmVec2 position;
    kmVec2 speed;
    kmVec2 accel;

    FacingDirection direction;

    bool walking_up;
    bool walking_down;
    bool walking_right;
    bool walking_left;

    bool walking_up_left;
    bool walking_up_right;
    bool walking_down_left;
    bool walking_down_right;

    bool standing_up;
    bool standing_down;
    bool stanting_right;
    bool standing_left;

    bool standing_up_left;
    bool standing_up_right;
    bool stanting_down_right;
    bool standing_down_left;

    
    bool attacking;
    
    bool exe_custom_behaviour;
  
    CollisionBox colBox;
    
    bool collided;

    /* Custom behaviours defined by scripts */
    ptrEntity_behaviour custom_behaviour;
    ptrEntity_collided custom_collide;
};


/* Movement Functions for entity */
extern void entity_move_up (EntityPtr entity);
extern void entity_move_down (EntityPtr  entity);
extern void entity_move_left (EntityPtr entity);
extern void entity_move_right (EntityPtr entity);
extern Animation* entity_get_current_animation(EntityPtr entity);
extern void entity_set_current_animation(EntityPtr entity, size_t anim_id );
/* extern void entity_move_up_left (EntityPtr entity);
 * extern void entity_move_up_right (EntityPtr  entity);
 * extern void entity_move_down_left (EntityPtr entity);
 * extern void entity_move_down_right (EntityPtr entity); */

/* Stop movement Functions for entity */
extern void entity_stop_move_up (EntityPtr entity);
extern void entity_stop_move_down (EntityPtr entity);
extern void entity_stop_move_left (EntityPtr entity);
extern void entity_stop_move_right (EntityPtr entity);
extern void entity_stop_move_up_left (EntityPtr entity);
extern void entity_stop_move_up_right (EntityPtr entity);
extern void entity_stop_move_down_left (EntityPtr entity);
extern void entity_stop_move_down_right (EntityPtr entity);
extern void entity_on_move(EntityPtr entity);

/* extern void entity_move(EntityPtr entity, int direction); */
/* extern void entity_stop_move(EntityPtr entity, int direction); */
extern void entity_stop(EntityPtr entity);


extern void entity_set_position(EntityPtr entity, float x, float y);

/* Event functions for entity */
extern void create_entity (char *name, EntityPtr entity);

extern void load_entity_from_xml(char *xml_file, EntityPtr entity);

extern void update_entity (EntityPtr entity);

extern void render_entity (double interpolation, EntityPtr entity);

extern void entity_execute_attack (EntityPtr entity);

extern void entity_collided(EntityPtr entity);


/* Util functions */
/* extern bool is_entity_moving (); */

/* extern void entity_fix_direction (); */


#endif // ENTITY_H_INCLUDED
