
#include "game.h"
#include "animation.h"

const AnimType STOP_ON_FINISH = 1;
const AnimType LOOP_ON_FINISH = 2;
const AnimType REVERSE_ON_FINISH = 3;


void
setup_animation_f (int max_frames, float frame_duration, TexturePtr texture, int strip_start_x, int strip_start_y,
                   int frame_width, int frame_height, Animation * animation, AnimType type)
{
    int i;

    animation->number_of_frames = max_frames;
    animation->frames = malloc (sizeof (TextureRegion) * max_frames);
    animation->frame_increment = 1;
    animation->frame_duration = frame_duration;
    animation->animation_finished = false;
    animation->old_time = 0;
    animation->current_frame = 0;
    animation->type = type;
    if (strip_start_x == 0)
        {
            for (i = 0; i < max_frames; i++)
                {
                    animation->frames[i] =
                        create_texture_region (texture, ((i == 0) ? 0 : frame_width * i), strip_start_y, frame_width,
                                               frame_height);
                }
        }

    else
        {
            for (i = (max_frames); i > 0; i--)
                {
                    animation->frames[i - 1] =
                        create_texture_region (texture, strip_start_x * i, strip_start_y, frame_width, frame_height);
                }
        }
}



void
setup_animation_p (int max_frames, float frame_duration, Animation * animation, AnimType type)
{
    animation->number_of_frames = max_frames;
    animation->frames = malloc (sizeof (TextureRegion) * max_frames);
    animation->frame_increment = 1;
    animation->frame_duration = frame_duration;
    animation->animation_finished = false;
    animation->old_time = 0;
    animation->current_frame = 0;
    animation->type = type;
}

void
setup_animation_a (TextureRegion frames[], int number_of_frames, float frame_duration, Animation * animation, AnimType type)
{
    animation->frames = frames;
    animation->frame_increment = 1;
    animation->number_of_frames = number_of_frames;
    animation->frame_duration = frame_duration;
    animation->animation_finished = false;
    animation->old_time = 0;
    animation->current_frame = 0;
    animation->type = type;
}

TextureRegionPtr
get_next_frame (Animation * animation)
{
    if (animation->number_of_frames == 1)
        {
            return &(animation->frames[0]);
        }


    double curr_time = glfwGetTime ();

    if ((animation->old_time + animation->frame_duration) > curr_time || animation->animation_finished)
        {

            return &(animation->frames[animation->current_frame]);

            //current_frame = &(animation->frames[animation->current_frame]);       //p_glt;
        }
    else
        {
            animation->old_time = curr_time;    /* al_get_timer_count (GAME_TIMER); */
            animation->current_frame += animation->frame_increment;

            if (animation->type == LOOP_ON_FINISH)
                {
                    if (animation->current_frame >= animation->number_of_frames)
                        animation->current_frame = 0;
                }
            else if (animation->type == STOP_ON_FINISH)
                {
                    if (animation->current_frame >= animation->number_of_frames)
                        {
                            animation->current_frame = animation->number_of_frames - 1;
                            animation->animation_finished = true;
                        }
                }
            else /* Means we are on REVERESE_ON_FINISH */
                {
                    if (animation->current_frame >= animation->number_of_frames || animation->current_frame <=0)
                        animation->frame_increment *= -1;
                }

           


            /* current_frame = &(animation->frames[animation->current_frame]); */
            return &(animation->frames[animation->current_frame]);;
        }
}

void
set_animation_to_frame (Animation * animation, int frame_number)
{
    if (frame_number >= animation->number_of_frames)
        {
           
            exit (-1);
        }

    animation->current_frame = frame_number;
}

void
reset_animation (Animation * animation)
{
    animation->current_frame = 0;
    animation->animation_finished = false;
}
