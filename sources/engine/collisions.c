#include "collisions.h"
#include <stdio.h>

void
create_collision_box(int x, int y, int width, int height, CollisionBoxPtr box)
{
    box->x = x;
    box->y = y;
    box->width = width;
    box->height = height;
}

bool
collide(CollisionBoxPtr box_a, CollisionBoxPtr box_b)
{
    int left1, left2;
    int right1, right2;
    int top1, top2;
    int bottom1, bottom2;
    
    left1 = box_a->x;
    left2 = box_b->x;

    right1 = left1 + box_a->width -1;
    right2 = left2 + box_b->width -1;

    bottom1 = box_a->y;
    bottom2 = box_b->y;

    top1 = bottom1 + box_a->height -1;
    top2 = bottom2 + box_b->height -1;

    
    if(top1 < bottom2) return false;
    if(bottom1 > top2) return false;

    if(right1 < left2) return false;
    if(left1 > right2) return false;

    return true;

}

