#ifndef ANIMATION_H_INCLUDED
#define ANIMATION_H_INCLUDED

#include "graphics.h"
#include <stdbool.h>

typedef struct Animation Animation;

typedef enum AnimationState AnimationState;

typedef int AnimType;


extern const AnimType STOP_ON_FINISH;
extern const AnimType LOOP_ON_FINISH;
extern const AnimType REVERSE_ON_FINISH;



enum AnimationState
    {  
        standing_up, standing_down, standing_left, standing_right,  walk_up, walk_down, walk_left, walk_right, attack_up, attack_left, attack_down, attack_right,
    };

struct Animation
{
    TextureRegion *frames;
    int number_of_frames;
    float frame_duration;       /*frame duration in secconds */
    int current_frame;
    double old_time;
    int frame_increment;
    bool animation_finished;
    AnimType type;
};

void setup_animation_p (int max_frames, float frame_duration, Animation * animation, AnimType type);

void setup_animation_a (TextureRegion frames[], int number_of_frames, float frame_duration, Animation * animation, AnimType type);

void setup_animation_f (int max_frames, float frame_duration, Texture * texture, int strip_start_x, int strip_start_y, int frame_width, int frame_height, Animation * animation, AnimType type);

TextureRegion *get_next_frame (Animation * animation);

void set_animation_to_frame (Animation * animation, int frame_number);

void reset_animation (Animation * animation);


#endif // ANIMATION_H_INCLUDED
