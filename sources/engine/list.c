#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "list.h"
#include "logging.h"





void
list_init (List * list, void (*destroy) (void *data))
{
    list->size = 0;
    list->destroy = destroy;
    list->head = NULL;
    list->tail = NULL;
}


ListElem * list_get(List *list, int index)
{
    int list_size = list_size(list);

    if(list_size == 0) 
        {
            log_message(ERROR, "list_get(): List is empty\n");
            exit(-1);
        }
    else if(index >= list_size)
        {
            log_message(ERROR, "list_get(): Index out of bounds\n");
            exit(-1);
        }
    
    /* get head */
    if(index == 0)
        {
            return list->head;
        }
    else if(index+1 == list_size)
        {
            return list->tail;
        }
    else 
        {
            int counter = 0;


            ListElem* element = list->head;
            while(counter < index){
                element = element->next;
                counter++;
            }
    
            return element; 
        }
    
}

void
list_destroy (List * list)
{
    void *data;

    /* Removes each element one by one */
    while (list_size (list) > 0)
        {

            if (list_remove (list, list_tail (list), (void **)&data) == 0 && list->destroy != NULL)
                {
                    /* Calls a user defined function to free dynamically allocated memory */
                    list->destroy (data);
                }
        }

    /* No operations are allowed now, but clear the structure as a precaution. */
    memset (list, 0, sizeof (List));
}




int
list_insert_next (List * list, ListElem * element, const void *data)
{
    ListElem *new_element;


    /* Do not allow a NULL element unless the list is empty */
    if (element == NULL && list_size (list) != 0)
        {
            return -1;
        }


    /* Allocates storage for the element */
    if ((new_element = (ListElem *) malloc (sizeof (ListElem))) == NULL)
        {
            return -1;
        }

    /* Insert element into the list */
    new_element->data = (void *)data;

    if (list_size (list) == 0)
        {
            list->head = new_element;
            list->head->previous = NULL;
            list->head->next = NULL;
            list->tail = new_element;
        }
    else
        {
            new_element->next = element->next;
            new_element->previous = element;

            if (element->next == NULL)
                {
                    list->tail = new_element;
                }
            else
                {
                    element->next->previous = new_element;
                }

            element->next = new_element;
        }

    /* if (element == NULL)
     *     {
     * 
     *         /\* Handle insertion at the head of the list *\/
     *         if (list_size (list) == 0)
     *             {
     *                 list->tail = new_element;
     *             }
     * 
     * 
     *         new_element->next = list->head;
     *         list->head = new_element;
     *     }
     * else
     *     {
     *         /\* Handle insertion somewhere other than the head *\/
     *         /\* If element->next is null it means we are at the tail *\/
     *         if (element->next == NULL)
     *             {
     *                 new_element->previous = list->tail;
     *                 list->tail = new_element;
     *             }
     * 
     *         new_element->next = element->next;
     *         element->next = new_element;
     *     } */

    list->size++;

    return 0;


}


int
list_ins_prev (List * list, ListElem * element, const void *data)
{
    ListElem *new_element;


    /* Do not allow a NULL element unless the list is empty */
    if (element == NULL && list_size (list) != 0)
        {
            return -1;
        }


    /* Allocates storage for the element */
    if ((new_element = (ListElem *) malloc (sizeof (ListElem))) == NULL)
        {
            return -1;
        }

    /* Insert element into the list */
    new_element->data = (void *)data;

    if (list_size (list) == 0)
        {
            /* Handle insertion when the list is empty */
            list->head = new_element;
            list->head->previous = NULL;
            list->head->next = NULL;
            list->tail = new_element;
        }
    else
        {
            new_element->next = element;
            new_element->previous = element->previous;

            if (element->previous == NULL)
                {
                    list->head = new_element;
                }
            else
                {
                    element->previous->next = new_element;
                }

            element->previous = new_element;
        }

    list->size++;

    return 0;
}



int
list_remove (List * list, ListElem * element, void **data)
{
    if (element == NULL || list_size (list) == 0)
        {
            return -1;
        }

    *data = element->data;

    if (element == list->head)
        {
            list->head = element->next;

            if (list->head == NULL)
                {
                    list->tail = NULL;
                }
            else
                {
                    element->next->previous = NULL;
                }
        }
    else
        {
            element->previous->next = element->next;

            if (element->next == NULL)
                {
                    list->tail = element->previous;
                }
            else
                {
                    element->next->previous = element->previous;
                }
        }

    free (element);

    list->size--;

    return 0;

    /* ListElem *old_element;
     * 
     * /\* Do not allow removal from an empty list *\/
     * if (list_size (list) == 0)
     *     {
     *         return -1;
     *     }
     * 
     * /\* Remove the element from the list *\/
     * if (element == NULL)
     *     {
     *         /\* Handle removal from the head of the list *\/
     *         *data = list->head->data;
     *         old_element = list->head;
     *         list->head = list->head->next;
     * 
     *         if (list_size (list) == 1)
     *             {
     *                 list->tail = NULL;
     *             }
     *     }
     * else
     *     {
     *         /\* Handle removal from somewhere other than the head *\/
     *         if (element->next == NULL)
     *             {
     *                 return -1;
     *             }
     * 
     *         *data = element->next->data;
     *         old_element = element->next;
     *         element->next = element->next->next;
     * 
     *         if (element->next == NULL)
     *             {
     *                 list->tail = element;
     *             }
     *     }
     * 
     * 
     * /\* Free the storage allocated by the abstract data type *\/
     * free (old_element);
     * 
     * /\* Adjust the size of the list to account for the removed element *\/
     * list->size--;
     * 
     * return 0; */
}


int
list_add (List * list,  const void *data)
{

    int ret = list_insert_next(list, list->tail, data);

    return ret;
    
}
