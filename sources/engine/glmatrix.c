#include "glmatrix.h"
void
matrixIdentity (mat4 m)
{
    m[0] = m[5] = m[10] = m[15] = 1.0;
    m[1] = m[2] = m[3] = m[4] = 0.0;
    m[6] = m[7] = m[8] = m[9] = 0.0;
    m[11] = m[12] = m[13] = m[14] = 0.0;
}

void
matrixTranslate (float x, float y, float z, mat4 matrix)
{
    matrixIdentity (matrix);

    // Translate slots.
    matrix[12] = x;
    matrix[13] = y;
    matrix[14] = z;
}

void
matrixScale (float sx, float sy, float sz, mat4 matrix)
{
    matrixIdentity (matrix);

    // Scale slots.
    matrix[0] = sx;
    matrix[5] = sy;
    matrix[10] = sz;
}

void
matrixRotateX (float degrees, mat4 matrix)
{
    float radians = degreesToRadians (degrees);

    matrixIdentity (matrix);

    // Rotate X formula.
    matrix[5] = cosf (radians);
    matrix[6] = -sinf (radians);
    matrix[9] = -matrix[6];
    matrix[10] = matrix[5];
}


void
matrixRotateY (float degrees, mat4 matrix)
{
    float radians = degreesToRadians (degrees);

    matrixIdentity (matrix);

    // Rotate Y formula.
    matrix[0] = cosf (radians);
    matrix[2] = sinf (radians);
    matrix[8] = -matrix[2];
    matrix[10] = matrix[0];
}

void
matrixRotateZ (float degrees, mat4 matrix)
{
    float radians = degreesToRadians (degrees);

    matrixIdentity (matrix);

    // Rotate Z formula.
    matrix[0] = cosf (radians);
    matrix[1] = sinf (radians);
    matrix[4] = -matrix[1];
    matrix[5] = matrix[0];
}

void
matrixMultiply (mat4 m1, mat4 m2, mat4 result)
{

    // Fisrt Column
    result[0] = m1[0] * m2[0] + m1[4] * m2[1] + m1[8] * m2[2] + m1[12] * m2[3];
    result[1] = m1[1] * m2[0] + m1[5] * m2[1] + m1[9] * m2[2] + m1[13] * m2[3];
    result[2] = m1[2] * m2[0] + m1[6] * m2[1] + m1[10] * m2[2] + m1[14] * m2[3];
    result[3] = m1[3] * m2[0] + m1[7] * m2[1] + m1[11] * m2[2] + m1[15] * m2[3];

    // Second Column    result[4] = m1[0]*m2[4] + m1[4]*m2[5] + m1[8]*m2[6] + m1[12]*m2[7];
    result[5] = m1[1] * m2[4] + m1[5] * m2[5] + m1[9] * m2[6] + m1[13] * m2[7];
    result[6] = m1[2] * m2[4] + m1[6] * m2[5] + m1[10] * m2[6] + m1[14] * m2[7];
    result[7] = m1[3] * m2[4] + m1[7] * m2[5] + m1[11] * m2[6] + m1[15] * m2[7];

    // Third Column
    result[8] = m1[0] * m2[8] + m1[4] * m2[9] + m1[8] * m2[10] + m1[12] * m2[11];
    result[9] = m1[1] * m2[8] + m1[5] * m2[9] + m1[9] * m2[10] + m1[13] * m2[11];
    result[10] = m1[2] * m2[8] + m1[6] * m2[9] + m1[10] * m2[10] + m1[14] * m2[11];
    result[11] = m1[3] * m2[8] + m1[7] * m2[9] + m1[11] * m2[10] + m1[15] * m2[11];

    // Fourth Column
    result[12] = m1[0] * m2[12] + m1[4] * m2[13] + m1[8] * m2[14] + m1[12] * m2[15];
    result[13] = m1[1] * m2[12] + m1[5] * m2[13] + m1[9] * m2[14] + m1[13] * m2[15];
    result[14] = m1[2] * m2[12] + m1[6] * m2[13] + m1[10] * m2[14] + m1[14] * m2[15];
    result[15] = m1[3] * m2[12] + m1[7] * m2[13] + m1[11] * m2[14] + m1[15] * m2[15];
}

void
setToProjection (float near_val, float far_val, float fov, float aspectRatio, mat4 matrix)
{
    matrixIdentity (matrix);
    float y_scale = (1 / tan (degreesToRadians ((fov / 2.0f))));
    float x_scale = y_scale / aspectRatio;
    float frustum_length = far_val - near_val;

    matrix[0] = x_scale;
    matrix[5] = y_scale;
    matrix[10] = -((far_val + near_val) / frustum_length);
    matrix[14] = -1;
    matrix[11] = -((2 * near_val * far_val) / frustum_length);
}


void
setToOrtho (float left, float right, float top, float bottom, mat4 matrix)
{
    float near_val = 0.0f;
    float far_val = 1.0f;

    matrixIdentity (matrix);

    /* These paramaters are lens properties.
       The "near" and "far" create the Depth of Field.
       The "left", "right", "bottom" and "top" represent the rectangle formed
       by the near area, this rectangle will also be the size of the visible area. */

    /*float left = 0.0, right = 320.0, bottom = 480.0, top = 0.0; */

    /* First Column */
    matrix[0] = 2.0 / (right - left);
    matrix[1] = 0.0;
    matrix[2] = 0.0;
    matrix[3] = 0.0;

    /* Second Column */
    matrix[4] = 0.0;
    matrix[5] = 2.0 / (top - bottom);
    matrix[6] = 0.0;
    matrix[7] = 0.0;

    /* Third Column */
    matrix[8] = 0.0;
    matrix[9] = 0.0;
    matrix[10] = 2.0 / (far_val - near_val);
    matrix[11] = 0.0;

    /* Fourth Column */
    matrix[12] = -(right + left) / (right - left);
    matrix[13] = -(top + bottom) / (top - bottom);
    matrix[14] = -(far_val + near_val) / (far_val - near_val);
    matrix[15] = 1;
}
