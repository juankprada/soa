#ifndef LIST_H_
#define LIST_H_

#include <stdlib.h>


/**
 * Define a structure for linked list elements
 */
typedef struct ListElem_
{
    void *data;


    struct ListElem_ *previous;
    struct ListElem_ *next;

} ListElem;


/**
 * Define a structure for a linked list 
 */
typedef struct List_
{
    int size;

    int (*match) (const void *key1, const void *key2);
    void (*destroy) (void *data);

    ListElem *head;
    ListElem *tail;

} List;


void list_init (List * list, void (*destroy) (void *data));

void list_destroy (List * list);

int list_insert_next (List * list, ListElem * element, const void *data);

int list_insert_prev (List * list, ListElem * element, const void *data);

int list_remove (List * list, ListElem * element, void **data);

int list_add (List * list, const void *data);

ListElem * list_get(List *list, int index);



#define list_size(list) ((list)->size)

#define list_tail(list) ((list)->tail)

#define list_head(list) ((list)->head)

#define list_is_head(list, element) ((element) == (list)->head ? 1 : 0)

#define list_is_tail(list, element) ((element) == NULL ? 1 : 0)

#define list_data(element) ((element)->data)

#define list_next(element) ((element)->next)

#define list_previous(element) ((element)->previous)


#endif /* LIST_H_ */
