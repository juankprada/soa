#include <stdio.h>
#include <stdarg.h>

#include "logging.h"

static const char* info_label = ">>> Info: ";
static const char* warning_label = ">>> Warning: ";
static const char* error_label = ">>> ERROR: ";


void
log_message(LogType level, const char* message, ...)
{
    register int i;
    va_list arg;


    va_start(arg, message);


    if(LOG_LEVEL == INFO && level == INFO)
        {
            fprintf(stdout, info_label);
            /* Print message on screen */
            vfprintf (stdout, message, arg);
            
        }
    else if(LOG_LEVEL == WARNING && level <= WARNING)
        {
            if(level == INFO)
                fprintf(stdout, info_label);
            else if(level == WARNING)
                fprintf(stdout, warning_label);

            /* Print message on screen */
            vfprintf(stdout, message, arg);
        }
    else if (LOG_LEVEL == ERROR && level <= ERROR)
        {
           

            /* Print message on screen */
            if(level == ERROR) 
                {
                    fprintf(stderr, error_label);
                    vfprintf(stderr, message, arg);   
                }
            else 
                {
                    if(level == INFO)
                        fprintf(stdout, info_label);
                    else if(level == WARNING)
                        fprintf(stdout, warning_label);

                    vfprintf(stdout, message, arg);
                }
        }
    
    va_end(arg);
}
