#include "camera.h"

static OrthoCamera *camera;

void
camera_create ()
{
    camera = (OrthoCamera *) malloc (sizeof (OrthoCamera));
}


void
camera_destroy ()
{
    free (camera);

}


void
camera_set_position_vec (kmVec2 pos)
{
    camera_set_position_coord (pos.x, pos.y);
}

void
camera_set_position_coord (float posx, float posy)
{

    int w, h;

    soa_get_resolution (&w, &h);

    float camera_posx = (w / 2) - posx;
    float camera_posy = (h / 2) - posy;


    camera->x = camera_posx;
    camera->y = camera_posy;


}


void camera_follow_entity(EntityPtr entity, double interpolation) 
{
    if(!entity->collided) 
        {
             camera_set_position_coord ((  (entity->position.x + (entity->speed.x * interpolation)) + (entity->current_frame.region_width / 2)),
                                        (  (entity->position.y + (entity->speed.y * interpolation)) + (entity->current_frame.region_height / 2)));
        }
    else 
        {
            camera_set_position_coord (
                                       (entity->position.x) + (entity->current_frame.region_width / 2),
                                       (entity->position.y) + (entity->current_frame.region_height / 2)
                                       );
        }
}

OrthoCamera *
camera_get_ortho_camera ()
{
    return camera;
}
