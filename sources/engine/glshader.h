

/*
 * glshader.h
 *
 *  Created on: Aug 19, 2012
 *      Author: juankprada
 */

#ifndef GLSHADER_H_
#define GLSHADER_H_


#include "../include/opengl.h"

#include "util.h"
typedef struct shader
{
    GLuint vertex_shader, fragment_shader, program;
} CGLShader;


//void show_info_log (GLuint object, PFNGLGETSHADERIVPROC glGet__iv, PFNGLGETSHADERINFOLOGPROC glGet__InfoLog);
CGLShader setup_shader ();
GLuint make_shader (GLenum type, const char *filename);

GLuint make_shader_program (GLuint vertex_shader, GLuint fragment_shader);

#endif /* GLSHADER_H_ */
