
#include <kazmath/kazmath.h>

#include "game.h"
#include "camera.h"
#include "spritebatch.h"

static int matrix_location;
static int texture_location;
static int in_position_location;
static int in_color_location;
static int in_text_coord_location;
static Texture *current_texture = NULL;


/* projection matrix*/

/*static mat4 projection_matrix;
static mat4 view_matrix;
static mat4 model_matrix;
static mat4 MVP;
*/

static kmMat4 projection_matrix;
static kmMat4 view_matrix;
static kmMat4 model_matrix;
static kmMat4 MVP;

static Color color;



/* Vertex and indices Buffer references */
static GLuint buffers[NUM_BUFFERS];



/* Shader program */
static CGLShader shader;
static bool blending_disabled = false;



/* Interleaved Vertex Array (VVCCCCTT) */
static GLfloat vertices[MAX_QUADS * VERTEX_ELEMENTS];
static int number_of_vertices = 0;


//static GLubyte indexes[MAX_QUADS * INDICES_PER_QUAD];
static int number_of_indices = 0;
static int render_calls = 0;
static int number_of_sprites_in_batch = 0;
static Color sb_render_color;
static float mov_camera_x = 0;



static void render_mesh ();

static char *default_vertex_shader = "vertex.glsl";
static char *default_fragment_shader = "fragment.glsl";

static float toRadians(float degrees) ;

void
sprite_batch_set_render_color (Color * color)
{
    sb_render_color.r = color->r;
    sb_render_color.g = color->g;
    sb_render_color.b = color->b;
    sb_render_color.a = color->a;
}


void
sprite_batch_setup ()
{
    shader = setup_shader (default_vertex_shader, default_fragment_shader);
    number_of_vertices = 0;
    number_of_indices = 0;
    number_of_sprites_in_batch = 0;
    glGenBuffers (NUM_BUFFERS, buffers);
    matrix_location = glGetUniformLocation (shader.program, "u_projectionViewMatrix");
    texture_location = glGetUniformLocation (shader.program, "texture_diffuse");
    in_position_location = glGetAttribLocation (shader.program, "in_Position");
    in_color_location = glGetAttribLocation (shader.program, "in_Color");
    in_text_coord_location = glGetAttribLocation (shader.program, "in_TextureCoord");
}


void
sprite_batch_enable_blending ()
{
    blending_disabled = false;
}


void
sprite_batch_disable_blending ()
{
    blending_disabled = true;
}


OrthoCamera *camera;

void
sprite_batch_begin ()
{
    int w = 800, h = 600;

    soa_get_resolution (&w, &h);

    /* Sets projection matrix to ortho */
    /*setToOrtho (0, w, h, 0, projection_matrix); */
    kmMat4Identity (&MVP);
    kmMat4Identity (&projection_matrix);
    kmMat4OrthographicProjection (&projection_matrix, 0, w, 0, h, 1, -1);


    /* Sets the view matrix */
    kmMat4Identity (&view_matrix);

    /* Translate the view_matrix in ortho mode to get a working camera */
    camera = camera_get_ortho_camera ();
    kmMat4Translation (&view_matrix, camera->x, camera->y, 0);

    /* The following commented code is used when in a Perspective projection and its useful for controlling cameras
       In an otrhographic projection, using the view_matrix translation is enoug */
    /* kmVec3 pEye;
     * kmVec3 pCenter;
     * kmVec3 pUp;
     * 
     * kmVec3Fill (&pEye, 0, 0, 1);
     * kmVec3Fill (&pCenter, 0, 0, 0);
     * kmVec3Fill (&pUp, 0, 1, 0); */

    /* kmMat4LookAt (&view_matrix, &pEye, &pCenter, &pUp); */

    /* Sets the model matrix as the identity */
    /*matrixIdentity (model_matrix); */
    kmMat4Identity (&model_matrix);

    /* mat4 PV; */
    kmMat4 PV;

    kmMat4Identity (&PV);
    kmMat4Multiply (&PV, &projection_matrix, &view_matrix);
    /* matrixMultiply (projection_matrix, view_matrix, PV); */
    kmMat4Multiply (&MVP, &PV, &model_matrix);



    /* Creates the MVP matrix to use in shader */
    /*matrixMultiply (PV, model_matrix, MVP); */

    glEnable (GL_TEXTURE_2D);
    if (blending_disabled)
        {
            glDisable (GL_BLEND);
        }

    else
        {
            glEnable (GL_BLEND);
            glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        }

    sb_render_color.r = 1;
    sb_render_color.g = 1;
    sb_render_color.b = 1;
    sb_render_color.a = 1;
}


void
sprite_batch_draw_region_r(TextureRegionPtr region, float posx, float posy, float degrees) 
{
     if (current_texture == NULL)
        {
            set_current_texture (region->texture);

        }
    else if (region->texture->texture_id != current_texture->texture_id)
        {
            render_mesh ();
            set_current_texture (region->texture);
        }

     int i = number_of_vertices;
     float fx = 0;
     float fy = 0;
     float fx2 = region->region_width - 0;
     float fy2 = region->region_height - 0;
     /* float fx2 = posx + region->region_width;
      * float fy2 = posy + region->region_height */;

    

     float p1x = fx;
     float p1y = fy;
     float p2x = fx;
     float p2y = fy2;
     float p3x = fx2;
     float p3y = fy2;
     float p4x = fx2;
     float p4y = fy;

     float x1;
     float y1;
     float x2;
     float y2;
     float x3;
     float y3;
     float x4;
     float y4;

     float rotation = toRadians(degrees);
     if(rotation != 0) 
         {
             float cosV = cos(rotation);
             float sinV = sin(rotation);
         
             x1 = cosV * p1x - sinV * p1y;
             y1 = sinV * p1x + cosV * p1y;
             
             x2 = cosV * p2x - sinV * p2y;
             y2 = sinV * p2x + cosV * p2y;
             
             x3 = cosV * p3x - sinV * p3y;
             y3 = sinV * p3x + cosV * p3y;
             
             x4 = x1 + (x3 - x2);
             y4 = y3 - (y2 - y1);
         } 
     else 
         {
             x1 = p1x;
             y1 = p1y;

             x2 = p2x;
             y2 = p2y;

             x3 = p3x;
             y3 = p3y;

             x4 = p4x;
             y4 = p4y;
         }

	x1 += posx;
    y1 += posy;
    x2 += posx;
    y2 += posy;
    x3 += posx;
    y3 += posy;
    x4 += posx;
    y4 += posy;
 
     /* vertex 1 */
    vertices[i++] = x1;
    vertices[i++] = y1;
    vertices[i++] = sb_render_color.r;
    vertices[i++] = sb_render_color.g;
    vertices[i++] = sb_render_color.b;
    vertices[i++] = sb_render_color.a;
    vertices[i++] = region->u;
    vertices[i++] = region->v;

    /* vertex 2 */
    vertices[i++] = x2;
    vertices[i++] = y2;
    vertices[i++] = sb_render_color.r;
    vertices[i++] = sb_render_color.g;
    vertices[i++] = sb_render_color.b;
    vertices[i++] = sb_render_color.a;
    vertices[i++] = region->u;
    vertices[i++] = region->v2;

    /* vertex 3 */
    vertices[i++] = x3;
    vertices[i++] = y3;
    vertices[i++] = sb_render_color.r;
    vertices[i++] = sb_render_color.g;
    vertices[i++] = sb_render_color.b;
    vertices[i++] = sb_render_color.a;
    vertices[i++] = region->u2;
    vertices[i++] = region->v2;

    /* vertex 4 */
    vertices[i++] = x4;
    vertices[i++] = y4;
    vertices[i++] = sb_render_color.r;
    vertices[i++] = sb_render_color.g;
    vertices[i++] = sb_render_color.b;
    vertices[i++] = sb_render_color.a;
    vertices[i++] = region->u2;
    vertices[i++] = region->v;

    number_of_vertices += VERTEX_ARRAY_ELEMENTS;
    number_of_indices += INDICES_PER_QUAD;
    number_of_sprites_in_batch++;
     
    render_mesh ();
     
}


float toRadians(float degrees) 
{
    return degrees * (M_PI/180);
}

void
sprite_batch_draw_region (TextureRegion * region, float posx, float posy)
{
    if (current_texture == NULL)
        {
            set_current_texture (region->texture);

        }
    else if (region->texture->texture_id != current_texture->texture_id)
        {
            render_mesh ();
            set_current_texture (region->texture);
        }

    int i = number_of_vertices;
    float fx2 = posx + region->region_width;
    float fy2 = posy + region->region_height;


    /* vertex 1 */
    vertices[i++] = posx;
    vertices[i++] = posy;
    vertices[i++] = sb_render_color.r;
    vertices[i++] = sb_render_color.g;
    vertices[i++] = sb_render_color.b;
    vertices[i++] = sb_render_color.a;
    vertices[i++] = region->u;
    vertices[i++] = region->v;

    /* vertex 2 */
    vertices[i++] = posx;
    vertices[i++] = fy2;
    vertices[i++] = sb_render_color.r;
    vertices[i++] = sb_render_color.g;
    vertices[i++] = sb_render_color.b;
    vertices[i++] = sb_render_color.a;
    vertices[i++] = region->u;
    vertices[i++] = region->v2;

    /* vertex 3 */
    vertices[i++] = fx2;
    vertices[i++] = fy2;
    vertices[i++] = sb_render_color.r;
    vertices[i++] = sb_render_color.g;
    vertices[i++] = sb_render_color.b;
    vertices[i++] = sb_render_color.a;
    vertices[i++] = region->u2;
    vertices[i++] = region->v2;

    /* vertex 4 */
    vertices[i++] = fx2;
    vertices[i++] = posy;
    vertices[i++] = sb_render_color.r;
    vertices[i++] = sb_render_color.g;
    vertices[i++] = sb_render_color.b;
    vertices[i++] = sb_render_color.a;
    vertices[i++] = region->u2;
    vertices[i++] = region->v;

    number_of_vertices += VERTEX_ARRAY_ELEMENTS;
    number_of_indices += INDICES_PER_QUAD;
    number_of_sprites_in_batch++;
}


void
sprite_batch_draw_texture (Texture * texture, float posx, float posy)
{
    if (current_texture == NULL)
        {
            set_current_texture (texture);
        }
    else if (texture->texture_id != current_texture->texture_id)
        {
            render_mesh ();
            set_current_texture (texture);
        }

    int i = number_of_vertices;


    /* vertex 1 */
    vertices[i++] = posx;
    vertices[i++] = posy;
    vertices[i++] = sb_render_color.r;
    vertices[i++] = sb_render_color.g;
    vertices[i++] = sb_render_color.b;
    vertices[i++] = sb_render_color.a;
    vertices[i++] = 0.0f;
    vertices[i++] = 0.0f;

    /* vertex 2 */
    vertices[i++] = posx;
    vertices[i++] = posy + texture->height;
    vertices[i++] = sb_render_color.r;
    vertices[i++] = sb_render_color.g;
    vertices[i++] = sb_render_color.b;
    vertices[i++] = sb_render_color.a;
    vertices[i++] = 0.0f;
    vertices[i++] = 1.0f;

    /* vertex 3 */
    vertices[i++] = posx + texture->width;
    vertices[i++] = posy + texture->height;
    vertices[i++] = sb_render_color.r;
    vertices[i++] = sb_render_color.g;
    vertices[i++] = sb_render_color.b;
    vertices[i++] = sb_render_color.a;
    vertices[i++] = 1.0f;
    vertices[i++] = 1.0f;

    /* vertex 4 */
    vertices[i++] = posx + texture->width;
    vertices[i++] = posy;
    vertices[i++] = sb_render_color.r;
    vertices[i++] = sb_render_color.g;
    vertices[i++] = sb_render_color.b;
    vertices[i++] = sb_render_color.a;
    vertices[i++] = 1.0f;
    vertices[i++] = 0.0f;
    number_of_vertices += VERTEX_ARRAY_ELEMENTS;
    number_of_indices += INDICES_PER_QUAD;
    number_of_sprites_in_batch++;
}



void
set_current_texture (Texture * texture)
{
    current_texture = texture;
}



void
render_mesh ()
{
    int i;
    int total_vertex = number_of_sprites_in_batch * VERTEX_ARRAY_ELEMENTS;

    GLfloat real_vertex[total_vertex];
    GLubyte real_indices[number_of_indices];

    for (i = 0; i < total_vertex; i++)
        {
            real_vertex[i] = vertices[i];
        }

    for (i = 0; i < number_of_indices; i++)
        {
            real_indices[i] = i;
        }
    glUseProgram (shader.program);
    glUniformMatrix4fv (matrix_location, 1, GL_FALSE, &MVP.mat[0]);

    if (current_texture != NULL)
        {
            glActiveTexture (GL_TEXTURE0);
        }

    glBindBuffer (GL_ARRAY_BUFFER, buffers[VERTICES]);
    glBufferData (GL_ARRAY_BUFFER, sizeof (real_vertex), real_vertex, GL_STATIC_DRAW);
    glVertexAttribPointer (in_position_location, 2, GL_FLOAT, GL_FALSE, 8 * sizeof (GLfloat), BUFFER_OFFSET (0));
    glEnableVertexAttribArray (in_position_location);
    glVertexAttribPointer (in_color_location, 4, GL_FLOAT, GL_FALSE, 8 * sizeof (GLfloat),
                           BUFFER_OFFSET (2 * sizeof (GLfloat)));
    glEnableVertexAttribArray (in_color_location);
    glVertexAttribPointer (in_text_coord_location, 2, GL_FLOAT, GL_FALSE, 8 * sizeof (GLfloat),
                           BUFFER_OFFSET (6 * sizeof (GLfloat)));
    glEnableVertexAttribArray (in_text_coord_location);
    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, buffers[INDICES]);
    glBufferData (GL_ELEMENT_ARRAY_BUFFER, sizeof (real_indices), real_indices, GL_STATIC_DRAW);
    glDrawElements (GL_QUADS, number_of_indices, GL_UNSIGNED_BYTE, 0);
    number_of_vertices = 0;
    number_of_indices = 0;
    number_of_sprites_in_batch = 0;
    render_calls++;
}



void
sprite_batch_end ()
{
    if (number_of_vertices >= VERTEX_ELEMENTS)
        {
            render_mesh ();
        }

    glDisableVertexAttribArray (in_position_location);
    glDisableVertexAttribArray (in_color_location);
    glDisableVertexAttribArray (in_text_coord_location);
    glUseProgram (0);
    glBindBuffer (GL_ARRAY_BUFFER, 0);
    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);

    glDisable (GL_BLEND);
    glDisable (GL_TEXTURE_2D);



    render_calls = 0;
}
