#ifndef GLMATRIX_H
#define GLMATRIX_H


#include <math.h>
#include "../include/opengl.h"

#define kPI180 0.017453

#define k180PI 57.295780

// Converts degrees to radians.
#define degreesToRadians(x) (x * kPI180)

// Converts radians to degrees.
#define radiansToDegrees(x) (x * k180PI)
typedef GLfloat mat4[16];
extern void matrixIdentity (mat4 m);
extern void matrixTranslate (float x, float y, float z, mat4 matrix);
extern void matrixScale (float sx, float sy, float sz, mat4 matrix);
extern void matrixRotateX (float degrees, mat4 matrix);
extern void matrixRotateY (float degrees, mat4 matrix);
extern void matrixRotateZ (float degrees, mat4 matrix);
extern void matrixMultiply (mat4 m1, mat4 m2, mat4 result);
extern void setToOrtho (float left, float right, float top, float bottom, mat4 matrix);
extern void setToProjection (float near, float far, float fov, float aspectRatio, mat4 matrix);


#endif // GLMATRIX_H
