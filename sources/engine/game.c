
/*
 *  game.c
 *
 *  Created on: Sep 16, 2012
 *  Author: Juankprada
 */


#include <iniparser.h>

#include "../include/opengl.h"
#include "game.h"
#include "defaults.h"
#include "gamestate.h"
#include "logging.h"

const int TICKS_PER_SECOND = 25;
const int SKIP_TICKS = 1000 / 25;
const int MAX_FRAME_SKIP = 5;

const int FPS = 60;

static unsigned int window_width = 800;
static unsigned int window_height = 480;

static unsigned int resolution_width = 800;
static unsigned int resolution_height = 480;


static unsigned int display_depth = 32;

static char *window_title = "Game Window";
static bool fullscreen = false;

static GameState *activeState = NULL;

static bool game_running = false;

/* static GameWindow *window; */

static void ExitGame (char *message);
static void soa_window_resize_callback (GLFWwindow * game_window, int w, int h);
static void init_opengl ();
static void error_callback (int error, const char *description);

static GLFWwindow *game_window = NULL;

static double next_game_tick;

GLFWwindow *
get_game_window ()
{
    return game_window;
}


void
soa_window_resize_callback (GLFWwindow * game_window, int w, int h)
{

    glfwGetFramebufferSize (game_window, &window_width, &window_height);
    glViewport (0, 0, (GLsizei) window_width, (GLsizei) window_height);
}


void
init_opengl ()
{
    GLenum err = glewInit ();

    if (GLEW_OK != err)
        {
            /* Problem: glewInit failed, something is seriously wrong. */
            log_message(ERROR, "Error initializing GLEW: %s\n", glewGetErrorString (err));
            return;
        }

    log_message(INFO, "Status: Using GLEW %s\n", glewGetString (GLEW_VERSION));


    /* set Clear color to a soft blue */
    glClearColor (0.4, 0.6, 0.9, 1);

    glfwGetFramebufferSize (get_game_window (), &window_width, &window_height);
    glViewport (0, 0, (GLsizei) window_width, (GLsizei) window_height);
}



void
soa_create_game_window ()
{

    dictionary *ini;

    /*"Specifies an error callback to check on any glfw error reported" */
    glfwSetErrorCallback (error_callback);

    /* Initialize GLFW library */
    if (glfwInit () == GL_FALSE)
        {
            ExitGame ("Error initializing sub-system\n");
        }

    /* Load default configuration file */
    ini = iniparser_load (CONFIG_FILE);
    if (ini == NULL)
        {
            log_message(ERROR,"cannot parse Config Gile [%s]\n", CONFIG_FILE);
            exit (EXIT_FAILURE);
        }

    /* get fullscreen flag from config file */
    fullscreen = iniparser_getboolean (ini, "window:FULLSCREEN", false);

    /* get window_width from config file */
    resolution_width = iniparser_getint (ini, "window:RES_WIDTH", 800);
    /* get window_height from config file */
    resolution_height = iniparser_getint (ini, "window:RES_HEIGHT", 480);

    display_depth = iniparser_getint (ini, "window:DIS_DEPT", 16);


    /* Create Windowed Display */
    if (!fullscreen)
        {
            /* Prevent the window from resizeing */
            glfwWindowHint (GLFW_RESIZABLE, GL_FALSE);

            window_width = resolution_width;
            window_height = resolution_height;

            game_window = glfwCreateWindow (window_width, window_height, window_title, NULL, NULL);
            if (!game_window)
                {
                    ExitGame ("Error Initializing Game Window\n");
                }

        }
    /* Create Fullscreen Display */
    else
        {
            GLFWmonitor *monitor = glfwGetPrimaryMonitor ();
            const GLFWvidmode *vid_mode = glfwGetVideoMode (monitor);

            window_width = vid_mode->width;
            window_height = vid_mode->height;

            game_window = glfwCreateWindow (window_width, window_height, window_title, monitor, NULL);
            if (!game_window)
                {
                    ExitGame ("Error Initializing Game Window\n");
                }

        }

    glfwMakeContextCurrent (game_window);

    glfwSetWindowSizeCallback (game_window, soa_window_resize_callback);

    init_opengl ();
}


double 
getTickCount()
{
    double val = glfwGetTime() * 1000;
   
    return val;
}

void
soa_run_game ()
{

    /* check for an active state */
    if (activeState == NULL)
        {
            ExitGame ("Error: No Game state has been set");
        }

    
    double interpolation = 0.0;
    double time_lapsed = 0.0;
    int loops = 0;
    float fpsTimer = getTickCount() + 1000;
    int fpsFrame = 1;
    int fps = 0;

    game_running = true;
 
    activeState->on_init ();
 
    next_game_tick = getTickCount();
    

    /* Main Game Loop */
    while (!glfwWindowShouldClose (game_window))
        {
            loops = 0;

            /* we get the elapsed millisecs since we started */
            
            while( getTickCount() > next_game_tick && loops < MAX_FRAME_SKIP)
                {
                    glfwPollEvents ();

                    /* update logic */
                    activeState->on_update ();
                    
                    next_game_tick += SKIP_TICKS;
                    loops++;
                    
                        
                }

           
            interpolation = (float)( getTickCount() + SKIP_TICKS - next_game_tick ) / (float)( SKIP_TICKS);
           
           
            /*  OpenGL rendering goes here.. */
            glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            activeState->on_render (interpolation);

            if(getTickCount() < fpsTimer)
                {
                    fpsFrame++;
                }
            else 
                {
                    fps = fpsFrame;
                    fpsFrame = 1;
                    fpsTimer = getTickCount() + 1000;
                }

            log_message(INFO, "FPS: %d/s\n", fps);

            /* Swap front and back rendering buffers */
            glfwSwapBuffers (game_window);
             /* Check if ESC key was pressed or window was closed */

            



        }

    /* Finalizes the active state to free resources */
    activeState->on_finish ();

    ExitGame ("Terminating");
}

void
soa_get_window_size (int *w, int *h)
{
    *w = window_width;
    *h = window_height;
    log_message(INFO, "GET WINDOW SIZE Screen sizes w:%d, h:%d\n", window_width, window_height);
}


void
soa_get_resolution (int *w, int *h)
{
    *w = resolution_width;
    *h = resolution_height;
}


void
soa_set_game_state (GameState * state)
{
    if (activeState == NULL)
        {
            activeState = malloc (sizeof (GameState));
        }

    soa_copy_game_state (activeState, state);
}

void
ExitGame (char *message)
{
    glfwDestroyWindow (game_window);
    game_window = NULL;
    log_message(ERROR, "%s\n", message);

    glfwTerminate ();
    exit (0);

}

void
error_callback (int error, const char *description)
{
    log_message(ERROR, "%s\n", description);
}
