#ifndef _COLISIONS_H
#define _COLISIONS_H

#include <stdbool.h>

typedef struct CollisionBox CollisionBox;

typedef struct CollisionBox* CollisionBoxPtr;

struct CollisionBox
{
    int x;
    int y;
    int width;
    int height;
};

void create_collision_box(int x, int y, int width, int height, CollisionBoxPtr box);


bool collide(CollisionBoxPtr box_a, CollisionBoxPtr box_b);




#endif
