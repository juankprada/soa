#ifndef CAMERA_H_
#define CAMERA_H_

#include "game.h"
#include "entity.h"
#include <kazmath/kazmath.h>

typedef kmVec2 OrthoCamera;


void camera_create ();
void camera_set_position_vec (kmVec2 pos);
void camera_set_position_coord (float posx, float posy);
void camera_follow_entity(EntityPtr entity, double interpolation);

OrthoCamera *camera_get_ortho_camera ();
void camera_destroy ();

#endif /* CAMERA_H_ */
