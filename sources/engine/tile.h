#ifndef _TILE_H_
#define _TILE_H_


#include "list.h"
#include "texture.h"

typedef struct _position Position;
typedef struct _tile Tile;
typedef struct _tilePosition TilePosition;


struct _tile
{
    int id = -1;

    List list_frames;
    TextureRegion *current_frame = NULL;

};


struct _tilePosition
{
    List list_positions;
    Tile *tile;
};


struct _postion
{
    int x = -1;
    int y = -1;
};


#endif
