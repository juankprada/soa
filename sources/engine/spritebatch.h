#ifndef SPRITE_BATCH_H_
#define SPRITE_BATCH_H_


#include "opengl.h"

#include "glshader.h"
#include "graphics.h"

#define MAX_QUADS   1000

#define VERTEX_ELEMENTS 8

#define VERTEX_ARRAY_ELEMENTS 32

#define INDICES_PER_QUAD 4

#define VERTICES    0
#define INDICES     1
#define NUM_BUFFERS 2


/* macro to calculate the offset of the vbo data */
#define BUFFER_OFFSET(bytes) ((GLubyte*) NULL + (bytes))


/* Color struct definition */
typedef struct Color Color;

struct Color
{
    float r;
    float g;
    float b;
    float a;
};



void sprite_batch_set_render_color (Color * color);
void sprite_batch_enable_blending ();
void sprite_batch_disable_blending ();
void sprite_batch_setup (void);
void sprite_batch_begin (void);
void sprite_batch_draw_texture (Texture * texture, float posx, float posy);
void sprite_batch_draw_region (TextureRegion * texture, float posx, float posy);
void sprite_batch_draw_region_r(TextureRegionPtr region, float posx, float posy, float degrees);
void sprite_batch_end (void);
void set_current_texture (Texture * texture);


#endif /* SPRITE_BATCH_H_ */
