#ifndef _FONTMANAGER_H_
#define _FONTMANAGER_H_

#include "../include/opengl.h"

#include "graphics.h"

typedef struct _font soaFont;


struct _font
{
    char *name;

    int size;

    Texture texture;
};


soaFont soa_create_font (const char *font_name, const char *bitmap_path);

void soa_draw_text (const soaFont * font, int posx, int posy, const char *text);



#endif
